-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2015 at 02:46 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sellproduct`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `idBanner` int(11) NOT NULL,
  `position` varchar(45) DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text,
  `link` text,
  `startDate` varchar(45) DEFAULT NULL,
  `endDate` varchar(45) DEFAULT NULL,
  `expiredDate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`idBanner`, `position`, `title`, `image`, `link`, `startDate`, `endDate`, `expiredDate`) VALUES
(1, 'Body Left', 'LG', '/uploads/files/LG.jpg', 'http://mp3.zing.vn', '11/01/2015', '11/30/2015', ''),
(2, 'Header Left', 'DaiKin', '/uploads/files/daikin_logo.jpg', 'http://mp3.zing.vn', '11/18/2015', '11/30/2015', ''),
(3, 'Body Left', 'DaiKin', '/uploads/files/daikin_logo.jpg', 'http://mp3.zing.vn', '11/17/2015', '11/30/2015', ''),
(4, 'Body Top', 'LG', '/uploads/files/LG.jpg', 'http://mp3.zing.vn', '11/01/2015', '11/17/2015', ''),
(5, '9', 'Tùng Hằng Coffee', '/sell-products-shop/CodeIgniter/uploads/images/tunghang.jpeg', 'None', '11/24/2015', '12/05/2015', ''),
(6, '11', 'For test', '/sell-products-shop/CodeIgniter/uploads/images/33.jpg', 'http://mp3.zing.vn', '11/19/2015', '12/01/2015', '');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `idCategories` int(11) NOT NULL,
  `categoryName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`idCategories`, `categoryName`) VALUES
(0, 'Tu Lanh'),
(1, 'Máy Giặt');

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE `information` (
  `idInformation` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `image` text,
  `created` varchar(255) DEFAULT NULL,
  `refLink` text,
  `type` varchar(45) DEFAULT NULL,
  `alias` text NOT NULL,
  `author` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hit` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`idInformation`, `title`, `description`, `image`, `created`, `refLink`, `type`, `alias`, `author`, `hit`) VALUES
(1, 'Giới thiệu', '<p><span style="line-height:1.6em">Năm 2010 L&ecirc; Thanh T&ugrave;ng th&agrave;nh lập T&ugrave;ng Hằng coffee tại Pleiku - một trong c&aacute;i n&ocirc;i của c&agrave; ph&ecirc; Việt Nam, với số vốn đầu ti&ecirc;n l&agrave; chiếc xe đạp cọc cạch cộng với niềm tin v&agrave; &yacute; ch&iacute; m&atilde;nh liệt của tuổi trẻ c&ugrave;ng với kh&aacute;t vọng x&acirc;y dựng một thương hiệu c&agrave; ph&ecirc; nổi tiếng, đưa hương vị c&agrave; ph&ecirc; Việt Nam lan tỏa khắp thế giới</span></p>', NULL, '1447741169', 'None', '0', 'gioi-thieu', 'Hai Bảy Năm', 0),
(2, 'Liên hệ', '<p>test cai thong bao the dc k adfas</p>', '', '1447490997', 'None', '0', 'lien-he', 'Hai Bảy Năm', 0),
(3, 'Chính sách', '<p>test</p>', '', '1447492137', 'None', '0', 'chinh-sach', 'Hai Bảy Năm', 0),
(4, 'Mục tiêu', '<p>Mục ti&ecirc;u v&agrave; hướng đi của doanh nghiệp</p>', '', '1447492074', 'None', '0', 'muc-tieu', 'Hai Bảy Năm', 0),
(25, 'Ứng dụng gỗ trong nội thất hiện đại', '<p><em>S&agrave;n gỗ, cửa gỗ, giường gỗ tủ bếp..một tổng thể nội thất bằng gỗ sẽ tạo cho kh&ocirc;ng gian sự mộc mạc m&agrave; sang trọng , qu&yacute; ph&aacute;i. Ở mỗi đồ nội thất ph&ugrave; hợp với một loại gỗ kh&aacute;c nhau n&ecirc;n nếu biết c&aacute;ch chọn tốt sẽ gi&uacute;p bạn c&oacute; được nội thất h&agrave;i h&ograve;a , tiết kiệm chi ph&iacute;.</em></p>\r\n\r\n<p><strong>1. S&agrave;n gỗ</strong><br />\r\nTrong những năm gần đ&acirc;y, ch&uacute;ng ta thường xuy&ecirc;n bắt gặp những s&agrave;n nh&agrave; bằng gỗ. S&agrave;n nh&agrave; bằng gỗ được sử dụng nhiều c&aacute;c ng&ocirc;i nh&agrave; mới nhằm tạo ra sự ấm &aacute;p trong m&ugrave;a đ&ocirc;ng v&agrave; sự m&aacute;t mẻ trong m&ugrave;a h&egrave;. Việc lắp s&agrave;n bằng gỗ c&oacute; thể khiến bạn dễ d&agrave;ng kết hợp với c&aacute;c vật dụng kh&aacute;c trong ph&ograve;ng. Bạn c&oacute; thể lựa chọn gỗ Căm xe, loại gỗ n&agrave;y c&oacute; m&agrave;u sắc tự nhi&ecirc;n, đẹp, ph&ugrave; hợp với nhiều kh&ocirc;ng gian cho s&agrave;n nh&agrave;, với m&agrave;u sắc đa dạng, v&acirc;n gỗ tự nhi&ecirc;n rất th&iacute;ch hợp với những gia đ&igrave;nh c&oacute; điều kiện. Hoặc gỗ Gi&aacute;ng hương đ&acirc;y l&agrave; loại gỗ sang trọng v&agrave; qu&yacute; ph&aacute;i, được khắp c&aacute;c nước &Acirc;u, &Aacute; ưa chuộng. Gi&aacute;ng hương l&agrave; loại gỗ c&oacute; m&ugrave;i thơm, v&acirc;n đẹp, thớ gỗ nhỏ v&agrave; rắn chắc n&ecirc;n l&agrave;m s&agrave;n nh&agrave; rất đẹp v&agrave; bền.</p>\r\n\r\n<p><img src="file:///C:/Users/Asus/Desktop/web1/web1/bizweb.dktcdn.net/100/022/409/files/cua-so-cho-khong-gian-noi-that-766e57a.jpg?v=1446641864637" style="height:auto" /></p>\r\n\r\n<p><strong>2. Giường gỗ</strong><br />\r\nGỗ pơ mu l&agrave; loại &nbsp;gỗ được sử dụng nhiều để l&agrave;m giường hiện nay, với m&agrave;u s&aacute;ng trắng, hoặc v&agrave;ng, v&acirc;n gỗ to v&agrave; đẹp th&igrave; gỗ pơ mu l&agrave; loại gỗ tốt, c&oacute; thớ mịn v&agrave; m&ugrave;i thơm đặc trưng của n&oacute; tốt cho sức khỏe. Ngo&agrave;i ra loại gỗ n&agrave;y c&oacute; thể tr&aacute;nh được muỗi v&agrave; c&ocirc;n tr&ugrave;ng, kh&ocirc;ng bị mối mai mọt ph&aacute; hoại.Ngo&agrave;i ra bạn c&oacute; thể chọn những loại s&agrave;n gỗ qu&yacute; c&oacute; gi&aacute; th&agrave;nh cao nhưng h&igrave;nh thức sang trọng: gụ, cẩm lai, mun sọc, mun đen, trắc</p>\r\n\r\n<p><strong>3. Cửa gỗ</strong><br />\r\nỞ đ&acirc;y, vật liệu d&agrave;nh cho cửa rất đa dạng, nhưng th&ocirc;ng dụng nhất hiện nay l&agrave; gỗ bởi vẻ đẹp sang trọng v&agrave; l&acirc;u bền của gỗ mang lại. C&aacute;c loại gỗ c&oacute; thể chịu mưa nắng v&agrave; bền đẹp với thời gian c&oacute; gỗ tr&ograve;, gỗ lim cho khu&ocirc;n cửa. Với cửa ở b&ecirc;n ngo&agrave;i, cửa ch&iacute;nh v&agrave; cửa sổ th&igrave; y&ecirc;u cầu độ bền của vật liệu rất cao bởi v&igrave; c&ocirc;ng năng sử dụng nhiều, v&agrave; hay phải chịu t&aacute;c động thường xuy&ecirc;n của mưa nắng, gi&oacute;.</p>\r\n\r\n<p><strong>4. Chọn gỗ cho nội thất ph&ograve;ng ăn</strong><br />\r\nPh&ograve;ng ăn l&agrave; nơi c&oacute; nhiệt độ ph&ograve;ng cao hơn, hay phải tiếp x&uacute;c với nước cho n&ecirc;n đồ gỗ trong bếp phải đ&aacute;p ứng được t&iacute;nh chịu nhiệt, chịu nước v&agrave; độ bền cao. Với những y&ecirc;u cầu sử dụng đ&oacute; th&igrave; việc lựa chọn loại gỗ n&agrave;o l&agrave; rất quan trọng. Sự lựa chọn tốt nhất cho gia đ&igrave;nh bạn ở đ&acirc;y l&agrave; gỗ dổi.</p>\r\n\r\n<p><img src="file:///C:/Users/Asus/Desktop/web1/web1/bizweb.dktcdn.net/100/022/409/files/noi-that-phong-ngub3df.jpg?v=1446641891307" style="height:auto" /></p>\r\n\r\n<p>Loại gỗ n&agrave;y c&oacute; độ bền cao, v&acirc;n tự nhi&ecirc;n v&agrave; sắc n&eacute;t, th&iacute;ch hợp với kh&iacute; hậu n&oacute;ng ẩm ở Việt Nam. Tủ bếp gỗ dổi thể hiện sự sang trọng, cổ điển m&agrave; vẫn to&aacute;t l&ecirc;n vẻ hiện đại v&agrave; c&oacute; độ bền tốt. Ngo&agrave;i ra, một số loại gỗ kh&aacute;c cũng hay được sử dụng như : sồi đỏ, sồi trắng, xoan, bạch t&ugrave;ng. Một loại gỗ kh&aacute; qu&yacute; hiếm rất th&iacute;ch hợp cho tủ bếp l&agrave; gi&aacute;ng hương.</p>', '/sell-products-shop/CodeIgniter/uploads/images/cua-so-cho-khong-gian-noi-that-766e57a.jpg', '1447491604', 'None', '1', 'ung-dung-go-trong-noi-that-hien-dai', 'Hai Bảy Năm', 0),
(26, 'Bố trí phòng khách nhà ống mang lại tài lộc cho gia chủ', '<p>Phong khach nha ong kh&ocirc;ng những l&agrave; nơi tiếp kh&aacute;ch m&agrave; c&ograve;n l&agrave; nơi c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh sum họp, qu&acirc;y quần b&ecirc;n nhau n&ecirc;n kh&ocirc;ng gian n&agrave;y được gia chủ đầu kh&aacute; kỹ lưỡng. Đặc biệt khi thiết kế nội thất ph&ograve;ng kh&aacute;ch nh&agrave; ống đ&uacute;ng phong thủy sẽ mang lại t&agrave;i lộc cho gia chủ.<br />\r\n&nbsp;<br />\r\nPhong thủy ph&ograve;ng kh&aacute;ch ch&uacute; trọng v&agrave;o việc bố tr&iacute;, sắp xếp nội thất. Theo quan niệm phong thủy nếu ng&ocirc;i nh&agrave; đảm bảo về mặt phong thủy th&igrave; sẽ mang đến cho gia chủ rất nhiều t&agrave;i lộc v&agrave; thuận lợi trong l&agrave;m ăn.</p>\r\n\r\n<p><img src="file:///C:/Users/Asus/Desktop/web1/web1/bizweb.dktcdn.net/100/022/409/files/ct100185.jpg?v=1446642160975" style="height:auto" /></p>\r\n\r\n<p><br />\r\n&nbsp;<br />\r\nCh&iacute;nh v&igrave; vậy m&agrave; Bosco sẽ chia sẻ cho bạn một v&agrave;i b&iacute; quyết bố tr&iacute; ph&ograve;ng kh&aacute;ch nh&agrave; ống m&agrave; mang lại t&agrave;i lộc cho chủ nh&acirc;n.<br />\r\n&nbsp;<br />\r\n<strong>Bố tr&iacute; ph&ograve;ng kh&aacute;ch</strong><br />\r\n&nbsp;<br />\r\nĐể thiết kế ph&ograve;ng kh&aacute;ch nh&agrave; ống hợp phong thủy th&igrave; n&ecirc;n bố tr&iacute; kh&ocirc;ng gian n&agrave;y ở vị tr&iacute; &ldquo;chỉ đạo&rdquo; để c&oacute; một tầm nh&igrave;n bao qu&aacute;t nhất trong ph&ograve;ng v&agrave; cửa ra v&agrave;o. Khi sắp xếp vị tr&iacute; c&oacute; tầm nh&igrave;n bao qu&aacute;t như vậy sẽ khiến cho bạn c&oacute; cảm gi&aacute;c c&acirc;n bằng, thoải m&aacute;i v&agrave; an t&acirc;m hơn cũng như đề ph&ograve;ng mọi bất trắc.<br />\r\n&nbsp;<br />\r\nHơn nữa, ph&ograve;ng kh&aacute;ch nh&agrave; ống cần được bố tr&iacute; ở những phương vị tốt như Thi&ecirc;n Y, Di&ecirc;n ni&ecirc;n, Sinh kh&iacute;.<br />\r\n&nbsp;<br />\r\n<strong>Bố tr&iacute; ghế ngồi trong ph&ograve;ng kh&aacute;ch</strong><br />\r\n&nbsp;<br />\r\nKhi bố tr&iacute; nội thất cho ph&ograve;ng kh&aacute;ch bạn cũng n&ecirc;n bố tr&iacute; ghế ngồi đối diện nhưng với một khoảng c&aacute;ch rộng để tạo n&ecirc;n sự trang trọng cho kh&ocirc;ng gian. Nhưng đối với những ng&agrave;y sum họp gia đ&igrave;nh bạn n&ecirc;n sắp xếp ghế chụm lại để tạo n&ecirc;n sự th&acirc;n mật gắn b&oacute; giữa c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh lại với nhau. Sự th&acirc;n mật n&agrave;y n&oacute;i l&ecirc;n sự thuận h&ograve;a trong gia đ&igrave;nh v&agrave; từ sự thuận h&ograve;a sẽ mang đến thịnh vượng, c&aacute;t tường cho gia chủ.<br />\r\n&nbsp;<br />\r\nTuy nhi&ecirc;n, bạn c&oacute; thể sắp xếp ghế ngồi theo h&igrave;nh chữ L đối với những ph&ograve;ng kh&aacute;ch nh&agrave; ống hẹp, hoặc l&agrave; theo h&igrave;nh b&aacute;t qu&aacute;t để tạo th&ecirc;m sự may mắn. Khi bạn bố tr&iacute; ghế ngồi theo h&igrave;nh b&aacute;t qu&aacute;t sẽ l&agrave;m cho c&aacute;c vị kh&aacute;ch c&oacute; cảm gi&aacute;c gần gũi v&agrave; mang lại kh&ocirc;ng kh&iacute; th&acirc;n mật khi tiếp kh&aacute;ch. Nhưng nhược điểm lớn của c&aacute;ch bố tr&iacute; ph&ograve;ng kh&aacute;ch nh&agrave; ống c&aacute;ch n&agrave;y đ&oacute; l&agrave; kh&aacute; tốn diện t&iacute;ch kh&ocirc;ng gian.</p>\r\n\r\n<p><img src="file:///C:/Users/Asus/Desktop/web1/web1/bizweb.dktcdn.net/100/022/409/files/noi-that-phong-ngub3df.jpg?v=1446641891307" style="height:auto" /></p>\r\n\r\n<p><br />\r\n<br />\r\n<strong>Bố tr&iacute; gương</strong></p>\r\n\r\n<p>Trong phong thủy nh&agrave; ở, gương c&oacute; t&aacute;c dụng điều h&ograve;a l&agrave;m cho sinh kh&iacute; lưu chuyển thuận lợi hơn. Kh&ocirc;ng những thế, gương c&ograve;n c&oacute; t&aacute;c dụng tăng th&ecirc;m &aacute;nh s&aacute;ng l&agrave;m cho căn ph&ograve;ng trở n&ecirc;n c&acirc;n đối, v&igrave; vậy m&agrave; bạn n&ecirc;n đặt c&aacute;c gương phản chiếu lớn. Tuy nhi&ecirc;n bạn cũng cần những lưu &yacute; quan trọng khi bố tr&iacute; gương trong ph&oacute;ng kh&aacute;ch nh&agrave; ống. Bạn kh&ocirc;ng n&ecirc;n gắn gương tr&ecirc;n trần ph&ograve;ng khiến người ngồi c&oacute; cảm gi&aacute;c bị đ&egrave; n&eacute;n, lộn ngược, kh&ocirc;ng thoải m&aacute;i. Cũng cần tr&aacute;nh việc treo hai gương đối diện nhau, điều n&agrave;y sẽ l&agrave;m kh&iacute; năng bị luẩn quẩn, nhiễu loạn, kh&oacute; tho&aacute;t.<br />\r\n&nbsp;<br />\r\nVới kinh nghiệm trong thiết kế nh&agrave; ở, nh&agrave; ống nhiều năm BoscoHome sẽ gi&uacute;p bạn triển khai những &yacute; tưởng thiết ph&ograve;ng kh&aacute;ch nh&agrave; ống đẹp, hợp phong thủy v&agrave; mang lại nhiều t&agrave;i lộc cho bạn.</p>', '/sell-products-shop/CodeIgniter/uploads/images/cua-so-cho-khong-gian-noi-that-766e57a.jpg', '1447492725', 'None', '1', 'bo-tri-phong-khach-nha-ong-mang-lai-tai-loc-cho-gia-chu', 'Hai Bảy Năm', 0),
(27, '3 ngôi nhà tre Việt Nam nổi tiếng thế giới', '<p>Bằng chất liệu tre, gỗ, c&aacute;c kiến tr&uacute;c sư Việt Nam đ&atilde; s&aacute;ng tạo n&ecirc;n những ng&ocirc;i nh&agrave;, qu&aacute;n c&agrave; ph&ecirc;, nh&agrave; h&agrave;ng đậm chất Việt được thế giới đ&aacute;nh gi&aacute; cao.<br />\r\nNhững c&ocirc;ng tr&igrave;nh kiến tr&uacute;c cổ nổi tiếng S&agrave;i G&ograve;n &nbsp;/ &nbsp;C&aacute;c nh&agrave; h&aacute;t kiến tr&uacute;c Ph&aacute;p trăm tuổi ở Việt Nam<br />\r\nSau đ&acirc;y l&agrave; ba c&ocirc;ng tr&igrave;nh kiến tr&uacute;c mang đậm hồn Việt được thế giới biết đến v&agrave; t&ocirc;n vinh trong l&agrave;ng thiết kế. Điểm đặc biệt của những c&ocirc;ng tr&igrave;nh n&agrave;y l&agrave; x&acirc;y dựng bằng tre, loại c&acirc;y phổ biến ở l&agrave;ng qu&ecirc; Việt Nam.</p>\r\n\r\n<h3>1. Nh&agrave; tre Trảng B&agrave;ng - T&acirc;y Ninh&nbsp;</h3>\r\n\r\n<p>Được x&acirc;y dựng từ năm 2008, sau hơn hai năm sau ng&ocirc;i nh&agrave; mới được ho&agrave;n th&agrave;nh, nằm ở huyện Trảng B&agrave;ng, tỉnh T&acirc;y Ninh, c&aacute;ch TP HCM 50 km. Với diện t&iacute;ch 2.000 m2, do &ocirc;ng Đặng Hảo (chủ nh&acirc;n) thiết kế, ng&ocirc;i nh&agrave; được l&agrave;m bằng vật liệu chủ yếu l&agrave; gỗ v&agrave; tre mua sẵn ở c&aacute;c v&ugrave;ng l&acirc;n cận, trong khi đ&oacute; m&aacute;i nh&agrave; lợp bằng l&aacute; dừa nước, một loại c&acirc;y rất phổ biến ở v&ugrave;ng Nam Bộ.&nbsp;</p>\r\n\r\n<p>Đến tham quan, du kh&aacute;ch sẽ nhận thấy sự n&ecirc;n thơ, thanh m&aacute;t với những b&ocirc;ng lục b&igrave;nh nhẹ tr&ocirc;i tr&ecirc;n d&ograve;ng s&ocirc;ng trước hi&ecirc;n nh&agrave;. C&ugrave;ng với đ&oacute; l&agrave; kh&ocirc;ng gian tho&aacute;ng đ&atilde;ng, h&agrave;i h&ograve;a khi ng&ocirc;i nh&agrave; được bao quanh bởi những đồng l&uacute;a xanh mướt với đ&agrave;n c&ograve; bay lượn, tạo n&ecirc;n vẻ đẹp quyến rũ, trữ t&igrave;nh.&nbsp;</p>\r\n\r\n<p>Ng&ocirc;i nh&agrave; l&agrave; ước mơ của những người y&ecirc;u th&iacute;ch lối sống thi&ecirc;n nhi&ecirc;n, thanh tịnh. Do đ&oacute; năm 2012 c&ocirc;ng tr&igrave;nh được rao b&aacute;n tr&ecirc;n tờ The Wall Street của Mỹ với gi&aacute; một triệu USD. Nhưng để sở hữu ng&ocirc;i nh&agrave;, người mua phải qua v&ograve;ng phỏng vấn đặc biệt của chủ nh&acirc;n về sở th&iacute;ch y&ecirc;u thi&ecirc;n nhi&ecirc;n v&agrave; lối sống th&acirc;n thiện với m&ocirc;i trường.&nbsp;</p>\r\n\r\n<h3><img src="file:///C:/Users/Asus/Desktop/web1/web1/bizweb.dktcdn.net/100/022/409/files/1-1762-1403838176ad00.jpg?v=1446526687133" style="height:auto" /><br />\r\n2. C&agrave; ph&ecirc; Gi&oacute; v&agrave; Nước, Thủ Dầu Một - B&igrave;nh Dương</h3>\r\n\r\n<p>Với sự s&aacute;ng tạo đặc biệt của kiến tr&uacute;c sư V&otilde; Trọng Nghĩa v&agrave; c&aacute;c cộng sự, qu&aacute;n c&agrave; ph&ecirc; Gi&oacute; v&agrave; Nước đ&atilde; được nhận giải thưởng Kiến tr&uacute;c quốc tế IAA 2008. Đ&acirc;y l&agrave; c&ocirc;ng tr&igrave;nh kiến tr&uacute;c độc đ&aacute;o được x&acirc;y dựng chủ yếu bằng c&acirc;y tầm v&ocirc;ng mọc nhiều ở B&igrave;nh Dương, tạo cảm gi&aacute;c như một đống rơm khổng lồ với c&aacute;c lối v&agrave;o nhỏ xinh.</p>\r\n\r\n<p>B&ecirc;n trong qu&aacute;n, nơi thưởng thức c&agrave; ph&ecirc; được đặt thấp hơn mặt nước, do đ&oacute; du kh&aacute;ch dễ d&agrave;ng cảm nhận l&agrave;n nước lững lờ theo cơn gi&oacute;. Những mảng c&acirc;y xanh cũng được đặt xen lẫn b&ecirc;n trong qu&aacute;n tạo n&ecirc;n một kh&ocirc;ng gian độc đ&aacute;o.&nbsp;</p>\r\n\r\n<p>Đến đ&acirc;y, du kh&aacute;ch vừa nh&acirc;m nhi ly c&agrave; ph&ecirc;, vừa được kh&aacute;m ph&aacute; c&ocirc;ng tr&igrave;nh kiến tr&uacute;c độc đ&aacute;o v&agrave; đắm m&igrave;nh trong kh&ocirc;ng gian xanh m&aacute;t chỉ c&oacute; gi&oacute; v&agrave; nước. Cuối tuần về với khoảng kh&ocirc;ng trầm lắng, b&ecirc;n tiếng nhạc du dương v&agrave; đ&oacute;n những cơn gi&oacute; m&aacute;t rượi l&agrave; một trải nghiệm đầy th&uacute; vị.</p>\r\n\r\n<h3><img src="file:///C:/Users/Asus/Desktop/web1/web1/bizweb.dktcdn.net/100/022/409/files/2-3168-1403838176a9a1.jpg?v=1446526700337" style="height:auto" /><br />\r\n3. Nh&agrave; h&agrave;ng tre Bamboo Wing, Đại Lải &ndash; Vĩnh Ph&uacute;c</h3>\r\n\r\n<p>Với điểm nhấn l&agrave; mặt hồ Đại Lải, Bamboo Wing được kiến tr&uacute;c sư V&otilde; Trọng Nghĩa thiết kế như hai c&aacute;nh hạc dang rộng bay v&uacute;t tr&ecirc;n mặt hồ, dựa tr&ecirc;n &yacute; tưởng từ những hoa văn tr&ecirc;n mặt trống đồng thời vua H&ugrave;ng dựng nước. Cũng như hai c&ocirc;ng tr&igrave;nh kiến tr&uacute;c tr&ecirc;n, vật liệu ch&iacute;nh của nh&agrave; h&agrave;ng l&agrave; tre mộc mạc gắn liền với qu&ecirc; hương, con người Việt Nam.</p>\r\n\r\n<p>Nh&igrave;n từ xa, Bamboo Wing được bao phủ bởi rừng c&acirc;y, lối v&agrave;o l&agrave; những tảng đ&aacute; được trải d&agrave;i tr&ecirc;n mặt nước. Ban ng&agrave;y, những tia nắng ban mai rọi xuống hồ Đại Lải như một mảng pha l&ecirc; lung linh ph&aacute;t s&aacute;ng khắp nh&agrave; h&agrave;ng v&agrave; kh&ocirc;ng gian xung quanh. Về đ&ecirc;m, những ngọn đ&egrave;n được đặt kh&eacute;o l&eacute;o trong ống tre ph&aacute;t s&aacute;ng, tạo n&ecirc;n một kh&ocirc;ng gian ấm &aacute;p v&agrave; lung linh huyền ảo, c&ugrave;ng trải nghiệm ho&agrave;n to&agrave;n kh&aacute;c lạ cho du kh&aacute;ch đến đ&acirc;y.</p>\r\n\r\n<p>Mới đ&acirc;y, nh&agrave; h&agrave;ng Bamboo Wing được nhận giải C&ocirc;ng tr&igrave;nh nghỉ dưỡng v&agrave; C&ocirc;ng tr&igrave;nh của năm do Hội kiến tr&uacute;c sư ch&acirc;u &Aacute; trao tặng.</p>\r\n\r\n<p><img src="file:///C:/Users/Asus/Desktop/web1/web1/bizweb.dktcdn.net/100/022/409/files/3-5510-14038381769fb2.jpg?v=1446526714754" style="height:auto" /></p>', '/sell-products-shop/CodeIgniter/uploads/images/cua-so-cho-khong-gian-noi-that-766e57a.jpg', '1447492776', 'None', '1', '3-ngoi-nha-tre-viet-nam-noi-tieng-the-gioi', 'Hai Bảy Năm', 0),
(28, 'Bố trí phòng khách nhà ống mang lại tài lộc cho gia chủ', '<p>Phong khach nha ong kh&ocirc;ng những l&agrave; nơi tiếp kh&aacute;ch m&agrave; c&ograve;n l&agrave; nơi c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh sum họp, qu&acirc;y quần b&ecirc;n nhau n&ecirc;n kh&ocirc;ng gian n&agrave;y được gia chủ đầu kh&aacute; kỹ lưỡng. Đặc biệt khi thiết kế nội thất ph&ograve;ng kh&aacute;ch nh&agrave; ống đ&uacute;ng phong thủy sẽ mang lại t&agrave;i lộc cho gia chủ.<br />\r\n&nbsp;<br />\r\nPhong thủy ph&ograve;ng kh&aacute;ch ch&uacute; trọng v&agrave;o việc bố tr&iacute;, sắp xếp nội thất. Theo quan niệm phong thủy nếu ng&ocirc;i nh&agrave; đảm bảo về mặt phong thủy th&igrave; sẽ mang đến cho gia chủ rất nhiều t&agrave;i lộc v&agrave; thuận lợi trong l&agrave;m ăn.</p>', '/sell-products-shop/CodeIgniter/uploads/images/cua-so-cho-khong-gian-noi-that-766e57a.jpg', '1447492838', 'None', '1', 'bo-tri-phong-khach-nha-ong-mang-lai-tai-loc-cho-gia-chu', 'Hai Bảy Năm', 0),
(29, 'Những loại cà phê ngon nhất thế giới', '<p>Thức uống m&agrave;u n&acirc;u đ&atilde; trở n&ecirc;n quen thuộc v&agrave; được y&ecirc;u th&iacute;ch tr&ecirc;n khắp thế giới. C&agrave; ph&ecirc; gi&uacute;p ch&uacute;ng ta thư gi&atilde;n thinh thần sau những giờ l&agrave;m việc mệt mỏi. Dưới đ&acirc;y l&agrave; một số loại c&agrave; ph&ecirc; ngon v&agrave; được ưa chuộng nhất tr&ecirc;n thế giới.</p>\r\n\r\n<p>1.&nbsp;&nbsp;&nbsp;Kopi Luwak: varying from $115,- to $590,- per 500 grams&nbsp; (caf&eacute; chồn)</p>\r\n\r\n<p>L&agrave; t&ecirc;n của một loại caf&eacute; đặc biệt, được xếp v&agrave;o h&agrave;ng sang trọng v&agrave; hiếm nhất thế giới. Loại caf&eacute; n&agrave;y hầu như chỉ c&oacute; ở Indonesia, tr&ecirc;n c&aacute;c h&ograve;n đảo Sumatra, Java v&agrave; Sulawesi.</p>\r\n\r\n<p>C&aacute;i t&ecirc;n gọi Kopi Luwak được d&ugrave;ng để chỉ một loại hạt do loại cầy v&ograve;i đốm ăn quả caf&eacute; rồi thải ra.</p>\r\n\r\n<p>Bắt nguồn từ từ kopi trong tiếng Indo c&oacute; nghĩa l&agrave; caf&eacute;. Luwak l&agrave; t&ecirc;n của một v&ugrave;ng thuộc đảo Java, đồng thời cũng l&agrave; t&ecirc;n của loại cầy cư tr&uacute; quoanh đ&oacute;. Lo&agrave;i cầy v&ograve;i đốm (Paradoxurus hermaphrodites) thuộc họ Cầy (Viverridae). Lo&agrave;i n&agrave;y ph&acirc;n bố rải r&aacute;c ở c&aacute;c nước v&ugrave;ng Đ&ocirc;ng Nam &Aacute; như Indonesia, Philippines, Việt Nam v&agrave; miền nam Trung Quốc. Thức uống ưa th&iacute;ch của ch&uacute;ng l&agrave; tr&aacute;i c&acirc;y v&agrave; quả caf&eacute;. Tuy nhi&ecirc;n, khi v&agrave;o dạ d&agrave;y, chỉ c&oacute; phần thịt caf&eacute; được ti&ecirc;u ho&aacute;, c&ograve;n hạt caf&eacute; lại theo đường ti&ecirc;u ho&aacute; bị thải ra ngo&agrave;i. Qu&aacute; tr&igrave;nh n&agrave;y tạo cơ sở ch&iacute;nh cho sự c&oacute; mặt của loại c&agrave; ph&ecirc; huyền thoại h&ocirc;m nay.</p>\r\n\r\n<p><img alt="" src="http://www.archcafe.net/data/image/kopi-luwak-coffee.jpg" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>2.&nbsp;&nbsp;&nbsp;Blue Mountain Coffee: $45,- per 500 grams</p>\r\n\r\n<p>&nbsp;C&agrave; ph&ecirc; Blue Mountain l&agrave; một trong những loại hạt c&agrave; ph&ecirc; Arabica c&oacute; gi&aacute; th&agrave;nh cao v&agrave; được ưa chuộng nhất tr&ecirc;n thế giới. N&oacute; c&oacute; nguồn gốc ở ph&iacute;a đ&ocirc;ng của d&atilde;y n&uacute;i Blue tr&ecirc;n Jamaica. Người ta gọi loại hạt c&agrave; ph&ecirc; n&agrave;y l&agrave;&nbsp;Jamaican Blue Mountain&nbsp;để ph&acirc;n biệt với những loại hạt c&agrave; ph&ecirc; kh&aacute;c.</p>\r\n\r\n<p>Với độ cao từ 2000m- 5000m, v&ugrave;ng n&uacute;i Blue Mountains l&agrave; một trong những v&ugrave;ng trồng c&agrave; ph&ecirc; cao nhất tr&ecirc;n thế giới. Kh&iacute; hậu ở đ&acirc;y dễ chịu, lượng mưa lớn, đất rất gi&agrave;u dinh dưỡng v&agrave; thấm nước tốt. Sự kết hợp giữa đất đai v&agrave; kh&iacute; hậu tạo n&ecirc;n điều kiện l&yacute; tưởng cho c&acirc;y c&agrave; ph&ecirc;. Tuy nhi&ecirc;n loại c&agrave; ph&ecirc; n&agrave;y kh&ocirc;ng th&iacute;ch hợp với c&aacute;c điều kiện kh&iacute; hậu kh&aacute;c. Sự thay đổi kh&iacute; hậu sẽ dẫn tới sự thay đổi hương vị c&agrave; ph&ecirc;. Ch&iacute;nh v&igrave; thế m&agrave; hiện nay n&oacute; mới chỉ được trồng ở Jamaica v&agrave; Hawaii.</p>\r\n\r\n<p>Theo những người s&agrave;nh c&agrave; ph&ecirc; th&igrave; loại c&agrave; ph&ecirc; n&agrave;y nổi tiếng với hương vị nhẹ, đượm m&ugrave;i, &iacute;t chua, c&oacute; ch&uacute;t x&iacute;u vị ngọt, đậm đ&agrave;. Gi&aacute; một kg c&agrave; ph&ecirc; loại n&agrave;y hiện nay khoảng 100 USD. Nhật Bản l&agrave; nước nhập khẩu c&agrave; ph&ecirc; Blue Mountain nhiều nhất (90% tổng sản lượng). Những hạt c&agrave; ph&ecirc; n&agrave;y cũng l&agrave; cơ sở cho c&aacute;c loại c&agrave; ph&ecirc; mang hương rượu Tia Maria.</p>\r\n\r\n<p>&nbsp;<img alt="" src="http://www.archcafe.net/data/image/tumblr_lqyk81lfmS1r2yvaro1_500.jpg" />&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>3.&nbsp;&nbsp;&nbsp;Bourbon Coffee:</p>\r\n\r\n<p>Bourbon l&agrave; giống c&agrave; ph&ecirc; được đặt t&ecirc;n bắt nguồn từ v&ugrave;ng đất sinh trưởng đầu ti&ecirc;n của n&oacute; đảo Bourbon, b&acirc;y giờ l&agrave; Reunion nằm ở ph&iacute;a đ&ocirc;ng Madagasca.&nbsp; Caf&eacute; Bourbon được sản xuất lần đầu ti&ecirc;n tại R&eacute;union, được biết đến như Bourbon Ile trước năm 1789. Sau đ&oacute; Bourbon được ưu th&iacute;ch bởi người Ph&aacute;p, Ch&acirc;u Phi, Ch&acirc;u Mỹ La Tinh, v&agrave; b&acirc;y giờ l&agrave; một trong hai loại c&agrave; ph&ecirc; Arabica được trồng phổ biến nhất tr&ecirc;n thế giới, l&agrave; một dạng của caf&eacute; Typica.</p>\r\n\r\n<p>Caf&eacute; Bourbon thường được sản xuất ở độ cao từ 1.000 đến 2.000 m&eacute;t v&agrave; cho ra năng suất cao hơn 20-30% so với Typica, nhưng c&oacute; thể tạo ra chất lượng caf&eacute; tương đương</p>\r\n\r\n<p>Loại caf&eacute; n&agrave;y c&oacute; vị chua thanh rất hấp dẫn, với m&ugrave;i thơm quyến rũ. Nhấp thử một ngụm, bạn sẽ thấy c&oacute; cảm gi&aacute;c thật th&iacute;ch th&uacute;. Bourbon đ&atilde; di thực v&agrave; v&agrave; được trồng ở miền cao nguy&ecirc;n Việt Nam từ rất l&acirc;u. Hiện nay, đ&acirc;y l&agrave; giống c&agrave; ph&ecirc; thơm ngon h&agrave;ng đầu của c&agrave; ph&ecirc; Việt Nam.</p>\r\n\r\n<p>&nbsp;4.&nbsp;&nbsp;&nbsp;Ethiopia:</p>\r\n\r\n<p>&nbsp;Ethiopia c&oacute; thể được v&iacute; l&agrave; c&aacute;i n&ocirc;i của c&agrave; ph&ecirc;. Trong thế kỷ thứ mười, người du mục đang chăn d&ecirc; tr&ecirc;n một sườn n&uacute;i Ethiopia đ&atilde; l&agrave; người đầu ti&ecirc;n nhận ra t&aacute;c dụng k&iacute;ch th&iacute;ch của c&agrave; ph&ecirc;. Sau đ&oacute; thứ thức uống n&agrave;y đ&atilde; được lan rộng tr&ecirc;n khắp Trung Đ&ocirc;ng bởi những người h&agrave;nh hương Sufi huyền b&iacute; mang đạo Hồi. Từ Trung Đ&ocirc;ng, giống caf&eacute; n&agrave;y dần được biết đến rộng r&atilde;i &nbsp;ở c&aacute;c nước Ch&acirc;u &Acirc;u v&agrave; Ch&acirc;u Mỹ.</p>\r\n\r\n<p>&nbsp;C&agrave; ph&ecirc; vẫn ph&aacute;t triển tự nhi&ecirc;n trong rừng n&uacute;i của Ethiopia. N&ocirc;ng d&acirc;n trồng c&agrave; ph&ecirc; Ethiopia trong bốn hệ thống kh&aacute;c nhau, trong đ&oacute; bao gồm c&agrave; ph&ecirc; rừng, c&agrave; ph&ecirc; b&aacute;n rừng, vườn c&agrave; ph&ecirc; v&agrave; trồng c&agrave; ph&ecirc;. Khoảng 98% lượng c&agrave; ph&ecirc; ở Ethiopia l&agrave; sản phẩm của n&ocirc;ng d&acirc;n tr&ecirc;n n&ocirc;ng trại nhỏ v&agrave; n&oacute; l&agrave; xuất khẩu quan trọng nhất của đất nước. Ethiopia l&agrave; nước sản xuất c&agrave; ph&ecirc; lớn thứ ba của ch&acirc;u Phi. C&oacute; khoảng 700.000 hộ sản xuất nhỏ c&agrave; ph&ecirc; ở Ethiopia, trong đ&oacute; 54 phần trăm l&agrave; trong rừng nửa diện t&iacute;ch. C&agrave; ph&ecirc; l&agrave; một phần của truyền thống văn h&oacute;a bản địa của họ trong hơn 10 thế hệ.</p>\r\n\r\n<p>&nbsp;C&agrave; ph&ecirc; Ethiopia l&agrave; một trong những nguồn gốc c&agrave; ph&ecirc; phổ biến nhất tr&ecirc;n thế giới. Tuy nhi&ecirc;n, Ethiopia phải cạnh tranh v&agrave; hợp t&aacute;c với c&aacute;c c&ocirc;ng ty c&agrave; ph&ecirc;, thường c&oacute; sức mạnh thị trường nhiều hơn v&agrave; kiếm được lợi nhuận cao hơn. H&agrave;ng năm, người d&acirc;n trồng c&agrave; ph&ecirc; ở Ethiopia trung b&igrave;nh kiếm được khoảng $ 900 mỗi năm. Chứng nhận c&agrave; ph&ecirc; Ethiopia bắt đầu sau khi th&agrave;nh lập Hội đồng C&agrave; ph&ecirc; của Ethiopia (NCBE) v&agrave;o năm 1957. Mục ti&ecirc;u của NCBE l&agrave; để kiểm so&aacute;t v&agrave; phối hợp sản xuất, kinh doanh, xuất khẩu v&agrave; n&acirc;ng cao chất lượng c&agrave; ph&ecirc; Ethiopia.</p>\r\n\r\n<p>&nbsp;M&ugrave;i hương của c&aacute;c giống c&agrave; ph&ecirc; bản địa Phi Ch&acirc;u phong ph&uacute; v&ocirc; kể, c&oacute; m&ugrave;i xen lẫn từ socola đến m&ugrave;i b&aacute;nh nướng, từ m&ugrave;i của đồng cỏ đến m&ugrave;i của hoa tr&aacute;i; vị từ ngọt ng&agrave;o cho đến chua thanh, từ đắng đến cay. Nơi đ&acirc;y quả kh&ocirc;ng hổ danh l&agrave; một vương quốc để suốt đời kh&aacute;m ph&aacute;.</p>\r\n\r\n<p><img alt="" src="http://www.archcafe.net/data/image/Coffee-1.jpg" />&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>5.&nbsp;&nbsp;&nbsp;Villasarchi:</p>\r\n\r\n<p>&nbsp;Villasarchi l&agrave; một giống lai của c&agrave; ph&ecirc; Bourbon. Villasarchi sinh trưởng ở thung lũng Sarchi ph&iacute;a T&acirc;y th&agrave;nh phố Costa Rica. C&aacute;c nh&aacute;nh của c&acirc;y c&agrave; ph&ecirc; n&agrave;y mọc xi&ecirc;n từ th&acirc;n một g&oacute;c 45 độ ph&acirc;n t&aacute;n một v&ugrave;ng l&aacute; bao phủ quanh c&acirc;y rất c&acirc;n đối. Villasarchi sinh trưởng tốt ở v&ugrave;ng đất c&oacute; độ cao lớn, dưới t&aacute;n c&acirc;y b&oacute;ng m&aacute;t v&agrave; giống c&agrave; ph&ecirc; n&agrave;y th&iacute;ch hợp với phương c&aacute;ch canh t&aacute;c hữu cơ, v&igrave; n&oacute; kh&ocirc;ng ưa c&aacute;c loại ph&acirc;n b&oacute;n h&oacute;a học. Tr&aacute;i của Villasarchi c&oacute; t&ocirc;ng m&agrave;u đỏ s&aacute;ng tr&ocirc;ng rất đẹp v&agrave; hấp dẫn, vị kh&aacute; chua một c&aacute;ch thanh tao xen lẫn với một độ đắng-ngọt rất huyền b&iacute;, tạo một cảm gi&aacute;c lạ khi uống.</p>\r\n\r\n<p>&nbsp;6.&nbsp;&nbsp;&nbsp;Typica:</p>\r\n\r\n<p>&nbsp;Arabica Typica l&agrave; giống c&agrave; ph&ecirc; l&acirc;u đời nhất, n&oacute; ch&iacute;nh l&agrave; giống c&agrave; ph&ecirc; đầu ti&ecirc;n được con người ph&aacute;t hiện ở v&ugrave;ng Kaffa của Ethiopia thế kỷ trước m&agrave; người ta hay kể như c&acirc;u chuyện của một ch&agrave;ng chăn d&ecirc; nọ. Hạt giống của c&agrave; ph&ecirc; Typica được mang đến Mỹ bởi một sĩ quan hải qu&acirc;n người Ph&aacute;p v&agrave;o những năm 1700. Typica c&oacute; năng suất rất thấp, k&iacute;ch thước hạt nhỏ, h&igrave;nh bầu dục. Tuy nhi&ecirc;n, chất lượng của Typica l&agrave; tuyệt vời thể hiện hương vị xuất sắc, vị đắng ngọt pha lẫn 1 ch&uacute;t chua nhẹ, thể chất mạnh v&agrave; qu&acirc;n b&igrave;nh.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;7.&nbsp;&nbsp;&nbsp;Geisha:</p>\r\n\r\n<p>&nbsp;Geisha l&agrave; một giống c&agrave; ph&ecirc; cực kỳ hiếm hoi m&agrave; đ&atilde; ho&agrave;n to&agrave;n chinh phục tất cả những người s&agrave;nh c&agrave; ph&ecirc; c&oacute; tin xuất sắc nhất tr&ecirc;n thế giới. Hương vị của n&oacute; phức tạp kh&ocirc;ng diễn tả nỗi. C&oacute; người cho rằng , n&oacute; rất phức tạp v&agrave; dữ dội! Giống c&agrave; ph&ecirc; nầy được ph&aacute;t hiện từ thị trấn nhỏ Gesha ở t&acirc;y nam Ethiopia v&agrave; được đưa đến trồng ở Costa Rica. Những c&acirc;y c&agrave; ph&ecirc; Geisha n&agrave;y ph&aacute;t triển rất cao, t&aacute;n đẹp, l&aacute; thu&ocirc;n d&agrave;i. Tr&aacute;i v&agrave; hạt c&agrave; ph&ecirc; Geisha n&agrave;y cũng d&agrave;i hơn so với c&aacute;c chủng loại c&agrave; ph&ecirc; kh&aacute;c. Chất lượng h&agrave;ng đầu của giống c&agrave; ph&ecirc; n&agrave;y được sinh ra từ c&aacute;c đồn điền tr&ecirc;n cao nguy&ecirc;n, v&agrave; được hoan ngh&ecirc;nh tr&ecirc;n to&agrave;n thế giới. Geisha c&oacute; vị ngọt ng&agrave;o, hậu vị chua thanh v&agrave; đắng dịu xen lẫn, hương vị phong ph&uacute; cực độ, thậm ch&iacute; c&oacute; cả m&ugrave;i tr&aacute;i c&acirc;y ch&iacute;n như xo&agrave;i, đu đủ, đ&agrave;o pha với mật ong, m&ugrave;i cỏ c&acirc;y, m&ugrave;i tr&aacute;i d&acirc;u rừng chung với m&ugrave;i đường mạch nha&hellip;</p>\r\n\r\n<p><img alt="" src="http://www.archcafe.net/data/image/Norway-Hacienda-La-Esmeralda-coffee-farm_sold-at-Thirsty-Sisters-3-Norway-roasters_10-13-10.jpg" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;8.&nbsp;&nbsp;&nbsp;Colombia:</p>\r\n\r\n<p>&nbsp;Giống c&agrave; ph&ecirc; Colombia, l&agrave; đứa con lai của hai bố mẹ Robusta v&agrave; Arabica. Giống c&agrave; ph&ecirc; nầy được g&acirc;y giống v&agrave; ph&aacute;t triển ở Colombia để chống lại bệnh trong khi tăng năng suất. Trong nhiều thập kỷ Colombia cố gắng cho ra đời h&agrave;ng chục phi&ecirc;n bản được gọi l&agrave; F10-F1. F10, c&ograve;n được gọi l&agrave; c&aacute;c Castillo. V&agrave; cuối c&ugrave;ng, hiện nay, đỉnh cao l&agrave; giống Colombia ch&iacute;nh thức, tự h&agrave;o mang t&ecirc;n 1 đất nước. Đ&acirc;y l&agrave; giống trồng phổ biến nhất lở Colambia. Mặc d&ugrave; Colombia c&oacute; m&ugrave;i hương thơm nồng nan, tuyệt vời, độ chua rất cao, xem với vị đắng c&oacute; hậu, hầu như kh&ocirc;ng thấy c&oacute; dư vị ngọt, thể chất mạnh, được sử dụng trong c&aacute;c thương hiệu c&agrave; ph&ecirc; cao cấp.</p>\r\n\r\n<p>&nbsp;C&agrave; ph&ecirc; Colombia l&agrave; một sự pha trộn của hạt c&agrave; ph&ecirc; từ nhiều khu vực kh&aacute;c nhau để duy tr&igrave; mức độ cao về chất lượng. Cũng như Kona v&agrave; Jamaican Blue Mountain, hạt c&agrave; ph&ecirc; Colombia được độc quyền từ c&agrave; ph&ecirc; Arabica, trong đ&oacute; c&oacute; một, hồ sơ hương vị &iacute;t t&iacute;nh axit mượt m&agrave; hơn so với hạt c&agrave; ph&ecirc; Robusta.</p>', '/sell-products-shop/CodeIgniter/uploads/images/about-us.jpg', '1447734719', 'None', '1', 'nhung-loai-ca-phe-ngon-nhat-the-gioi', '', 0),
(30, 'Nên làm gì khi nghe tin nước bạn mất mùa robusta?', '<p>Một nh&agrave; kinh doanh kể lại chuyến đi thực địa của m&igrave;nh sang c&aacute;c v&ugrave;ng c&agrave; ph&ecirc; Brazil v&agrave;o tuần trước như sau:</p>\r\n\r\n<p>&ldquo;Trong chuyến c&ocirc;ng t&aacute;c tại Brazil v&agrave;o tuần trước, đ&acirc;u đ&acirc;u thi&ecirc;n hạ đều cho rằng mưa đến thời điểm đấy l&agrave; tốt v&igrave; cứu được đợt hoa đầu ti&ecirc;n v&agrave; độ ẩm tại c&aacute;c v&ugrave;ng c&agrave; ph&ecirc; ch&egrave; arabica đều ở c&aacute;c mức l&yacute; tưởng. Nếu như mưa trễ th&ecirc;m chừng 2 tuần th&igrave; chắc t&igrave;nh h&igrave;nh sẽ kh&aacute;c, nhưng ngay tại thời điểm n&agrave;y (tuần trước) rất &iacute;t ai tin rằng kh&ocirc; hạn l&agrave;m hại sản lượng c&agrave; ph&ecirc; ch&egrave; arabica. Ri&ecirc;ng v&ugrave;ng c&agrave; ph&ecirc; robusta hay người Brazil thường gọi l&agrave; &ldquo;conillon&rdquo; như tại Việt Nam gọi l&agrave; c&agrave; ph&ecirc; &ldquo;vối&rdquo; th&igrave; kh&aacute;c, chỉ mới c&oacute; mưa v&agrave; h&igrave;nh như sẽ g&acirc;y t&aacute;c hại tr&ecirc;n sản lượng loại n&agrave;y. N&oacute;i giống như n&ocirc;ng d&acirc;n ở Indonesia n&oacute;i hoa &ldquo;rụng v&agrave; ch&aacute;y&rdquo; cả, th&igrave; mấy bữa r&agrave;y gi&aacute; tr&ecirc;n s&agrave;n kỳ hạn c&agrave; ph&ecirc; vối robusta London kh&ocirc;ng rớt mạnh cũng muốn b&aacute;o rằng sắp tới sản lượng robusta sẽ &iacute;t hơn.&rdquo;</p>\r\n\r\n<p><img alt="" src="http://24htaynguyen.com/wp-content/uploads/2014/10/gia-ca-phe.jpg" style="margin:0px" /></p>\r\n\r\n<p>Xuất khẩu c&agrave; ph&ecirc; vối robusta từ Việt Nam&nbsp;mấy th&aacute;ng r&agrave;y cũng hạn chế n&ecirc;n thị trường cũng chẳng d&aacute;m xuống s&acirc;u, d&ugrave; cho tin đồn vẫn huy&ecirc;n thuy&ecirc;n rằng tồn kho gối vụ c&ograve;n lớn. N&ecirc;n, nếu như mới đ&acirc;y gi&aacute; kỳ hạn London chịu sức &eacute;p t&acirc;m l&yacute; nặng nề th&igrave; nay thị trường c&oacute; ki&ecirc;ng d&egrave; hơn do tin hạn h&aacute;n tr&ecirc;n c&aacute;c v&ugrave;ng c&agrave; ph&ecirc; vối robusta tại Brazil.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, bạn đọc cũng cần lưu &yacute; rằng th&ocirc;ng tin sản lượng đ&ocirc;i khi chỉ l&agrave; b&agrave;i lừa dư luận để đ&aacute;nh &ldquo;mặt trận t&agrave;i ch&iacute;nh&rdquo;, n&ecirc;n hết sức thận trọng v&igrave; đ&ocirc;i khi nước cạnh tranh muốn gi&agrave;nh lại mạnh thị trường đ&atilde; bị mất từ tay Việt Nam từ mươi năm nay.</p>\r\n\r\n<blockquote>\r\n<p>Dữ liệu xuất khẩu c&agrave; ph&ecirc; Brazil vừa cho biết xuất khẩu c&agrave; ph&ecirc; Brazil trong th&aacute;ng 10-2015 đạt 3,31 triệu bao, cao hơn so với th&aacute;ng 9-2015 l&agrave; 2,92 triệu bao v&agrave; cũng cao vượt so với c&ugrave;ng kỳ năm 2014 bấy giờ l&agrave; 3,09 triệu bao. Đ&acirc;y l&agrave; dẫn chứng h&ugrave;ng hồn nhất m&agrave; ta dựa v&agrave;o đ&oacute; để đ&aacute;nh gi&aacute; sản lượng của họ, cao hơn những con số tr&ecirc;n c&aacute;c phương tiện th&ocirc;ng tin đại ch&uacute;ng họ muốn loan truyền.</p>\r\n</blockquote>\r\n\r\n<p>Reuters tuần trước cũng b&aacute;o rằng&nbsp;Brazil đẩy mạnh xuất khẩu c&agrave; ph&ecirc; vối sang ch&acirc;u &Acirc;u, kể cả Italia l&agrave; thị trường truyền thống của hột c&agrave; ph&ecirc; vối Việt Nam.</p>\r\n\r\n<blockquote>\r\n<p>Trong khi th&ocirc;ng tin chưa được kiểm chứng, nhiều người tr&ecirc;n thị trường nội địa đ&atilde; từng thua đau trong năm vừa qua do qu&aacute; tin v&agrave;o c&aacute;c bản tin hạn h&aacute;n, mất m&ugrave;a, thiết nghĩ n&ecirc;n thấy được gi&aacute; l&agrave; b&aacute;n một phần sản lượng m&igrave;nh sản xuất ra chứ kh&ocirc;ng n&ecirc;n chờ gi&aacute; đỉnh v&igrave; biết đ&acirc;u l&agrave; đỉnh. B&aacute;n trừ l&ugrave;i hay cộng tới, n&ecirc;n t&igrave;m c&aacute;ch chốt gi&aacute; kh&ocirc;ng để chần chờ v&igrave; rất kh&oacute; m&agrave; ăn d&agrave;y đối với người đ&atilde; cầm h&agrave;ng của bạn trong tay. Thực ra, người mua kh&ocirc;ng muốn bạn chốt lắm đ&acirc;u do c&ograve;n muốn chiếm dụng vốn v&igrave; chỉ thanh to&aacute;n 70% khi giao h&agrave;ng.</p>\r\n</blockquote>\r\n\r\n<p>T&iacute;nh đến hết ng&agrave;y 27-10, vị thế kinh doanh tổng hợp gồm cả kỳ hạn v&agrave; quyền chọn của c&aacute;c quỹ đầu cơ tr&ecirc;n hai s&agrave;n kỳ hạn như sau đều tăng b&aacute;n r&ograve;ng:</p>\r\n\r\n<p>S&agrave;n Ice New York cho arabica tăng lượng b&aacute;n r&ograve;ng l&ecirc;n 13606 l&ocirc; từ 4489 l&ocirc; tuần trước, tăng 9117 l&ocirc;.</p>\r\n\r\n<p>S&agrave;n Ice London cho robusta cũng tăng b&aacute;n r&ograve;ng th&ecirc;m 904 l&ocirc; từ 10906 l&ocirc; tuần trước đ&oacute; l&ecirc;n 11810 l&ocirc; ng&agrave;y kh&oacute;a sổ 27-10.</p>\r\n\r\n<p>Gi&aacute; c&agrave; ph&ecirc; tr&ecirc;n 2 s&agrave;n kỳ hạn l&agrave; 7 trong 10 s&agrave;n kỳ hạn n&ocirc;ng sản c&oacute; gi&aacute; tăng h&ocirc;m qua. Gi&aacute; s&agrave;n arabica đạt 123.60 cts/lb tăng 1.45 cts/lb trong khi robusta London chốt mức 1645 USD/tấn, tăng 16 USD/tấn. London đ&oacute;ng cửa nằm tr&ecirc;n mức kh&aacute; an to&agrave;n. London cần t&iacute;ch lũy ở c&aacute;c mức n&agrave;y để t&igrave;m cơ hội tăng.</p>\r\n\r\n<p><em><strong>Dự kiến s&agrave;n robusta&nbsp;</strong></em><strong><em>Ice&nbsp;</em></strong><strong><em>robusta London th&aacute;ng 1-2016&nbsp;&nbsp;</em></strong><em><strong>mở cửa chiều&nbsp;</strong></em><em><strong>4-11 từ giảm kh&aacute; đến giảm nhẹ.</strong></em></p>', '/sell-products-shop/CodeIgniter/uploads/images/1_zpsed262367.jpg', '1447736239', 'None', '1', 'nen-lam-gi-khi-nghe-tin-nuoc-ban-mat-mua-robusta', '', 0),
(31, 'Những hình ảnh đẹp về cây cà phê', '<p>test</p>', '/sell-products-shop/CodeIgniter/uploads/images/CAY-CA-PHE-CHE-2.jpg', '1447820432', 'None', '1', 'nhung-hinh-anh-dep-ve-cay-ca-phe', 'Admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `idOrder` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `typePayment` varchar(45) DEFAULT NULL,
  `created` varchar(45) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `status` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discountCode` varchar(45) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `idPartner` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `link` text NOT NULL,
  `image` text NOT NULL,
  `created` varchar(45) NOT NULL,
  `updateAt` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`idPartner`, `name`, `link`, `image`, `created`, `updateAt`) VALUES
(2, 'Công ty ABC', 'http://mp3.zing.vn', '/sell-products-shop/CodeIgniter/uploads/images/unnamed.png', '1447646708', '1447647774'),
(3, 'web-cong-ty', 'http://mp3.zing.vnsdjfhsjdfakshdfkhaskdfhkajshdfkjshdfkjhaskdjfhaskjdf', '/sell-products-shop/CodeIgniter/uploads/images/cua-so-cho-khong-gian-noi-that-766e57a.jpg', '1447648104', ''),
(4, 'Công ty BC', 'http://news.zing.vn', '/sell-products-shop/CodeIgniter/uploads/images/cua-so-cho-khong-gian-noi-that-766e57a.jpg', '1447653808', '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `idProducts` int(11) NOT NULL,
  `productName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` varchar(45) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `image` text,
  `detail` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `discount` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`idProducts`, `productName`, `categoryId`, `price`, `created`, `alias`, `description`, `image`, `detail`, `discount`) VALUES
(1, 'Tu Lanh gia re', 0, '1.200,00', '1447387243', 'tu-lanh-gia-re', 'abc', '/sell-products-shop/CodeIgniter/uploads/files/f28653b518fff1a1a8ee.png', '<p>abc</p>\r\n', '10'),
(2, 'Máy giặt cũ giá rẻ', 0, '2.000,00', '1447389165', 'may-giat-cu-gia-re', 'May chay kha tot', '/sell-products-shop/CodeIgniter/uploads/files/sua-may-giat-cua-ngang.jpg', '<p>abcbcb</p>\r\n', '0'),
(3, 'Máy giặt cũ giá rẻ bá đạo', 0, '1.500,00', '1447389226', 'may-giat-cu-gia-re-ba-dao', 'Chất lượng cao', '/sell-products-shop/CodeIgniter/uploads/files/dich-vu-sua-chua-may-giat-tai-nha-3.jpg', '<p>chat luog cao</p>\r\n', '0'),
(4, 'Máy giặt cũ gia binh dan', 0, '2.500,00', '1447389633', 'may-giat-cu-gia-binh-dan', 'Máy chạy rất tốt, ben', '/sell-products-shop/CodeIgniter/uploads/files/680_P_1364333524162.jpg', '<p>ababa</p>\r\n', '10'),
(5, 'Tu Lanh Panasonic hang moi ve', 0, '3.000,00', '1447389643', 'tu-lanh-panasonic-hang-moi-ve', 'Kieu dang thoi trang', '/sell-products-shop/CodeIgniter/uploads/files/dich-vu-sua-chua-may-giat-tai-nha-3.jpg', '<p>Kieu dang thoi trang</p>\r\n', '5'),
(6, 'Supper máy giặt', 0, '1.200,00', '1447388629', 'supper-may-giat', 'No thing', '/sell-products-shop/CodeIgniter/uploads/files/sua-may-giat-cua-ngang.jpg', '<p>Test</p>\r\n', '0');

-- --------------------------------------------------------

--
-- Table structure for table `usergruop`
--

CREATE TABLE `usergruop` (
  `idUserGruop` int(11) NOT NULL,
  `groupName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usergruop`
--

INSERT INTO `usergruop` (`idUserGruop`, `groupName`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(45) NOT NULL,
  `created` varchar(255) NOT NULL,
  `groupID` varchar(45) NOT NULL,
  `addressPayment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `addressShip` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `phoneNumber` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_code` varchar(45) DEFAULT NULL,
  `avatar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUsers`, `password`, `fullName`, `email`, `status`, `created`, `groupID`, `addressPayment`, `addressShip`, `phoneNumber`, `verification_code`, `avatar`) VALUES
(2, 'c9eb43a0a5a7a6c84b1f33188fb7b14bf62e0996bee3f10b22ddff64dae28eee', 'Le Anh Sy', 'leanhsy@gmail.com', '0', '1444272053', '2', NULL, NULL, '+84935486279', '0a42f2e100e8baf81f2616bae67bf56d9190de81b7f9c', '/uploads/images/unnamed.png'),
(3, 'c9eb43a0a5a7a6c84b1f33188fb7b14bf62e0996bee3f10b22ddff64dae28eee', 'Tharin', 'anh@gmail.com', '0', '1444272085', '3', NULL, NULL, '+84935478389', '99a5028a06a88e0efdf1b2bb0bb5f2e41ae60eaebc921', ''),
(4, 'c9eb43a0a5a7a6c84b1f33188fb7b14bf62e0996bee3f10b22ddff64dae28eee', 'Sirius', 'sy@gmail.com', '0', '1444272105', '3', NULL, NULL, '+84945384374', '4b4b0d12ffd054ba86737ad7d00e6955b303d715d50ec', ''),
(5, 'c9eb43a0a5a7a6c84b1f33188fb7b14bf62e0996bee3f10b22ddff64dae28eee', 'Violet', 'asd@gmail.com', '0', '1444276554', '1', NULL, NULL, '+84903314675', 'f279032bcad92c17e248e93403eda29515ad3b3241be4', ''),
(7, 'c9eb43a0a5a7a6c84b1f33188fb7b14bf62e0996bee3f10b22ddff64dae28eee', 'Hung Hoang', 'cncnc@gmail.com', '0', '1444276612', '3', NULL, NULL, '+84909888888', '5bae4d03d377563356d649061a217115116b8823daa35', ''),
(8, 'c9eb43a0a5a7a6c84b1f33188fb7b14bf62e0996bee3f10b22ddff64dae28eee', 'Peter', 'ab@gmail.com', '0', '1444277644', '2', NULL, NULL, '+84903323029', '6640ee269c9d76d9cbf6f706e572bd39c85c763aefe0a', ''),
(9, 'c9eb43a0a5a7a6c84b1f33188fb7b14bf62e0996bee3f10b22ddff64dae28eee', 'Hai Bay Nam', 'haibaynam81@gmail.com', '0', '1447023805', '1', NULL, NULL, '+84989859170', 'f4f73dfa9ade587c926288736a87c6e0cbff225fd7c89', ''),
(10, 'ad969bf0fdd6184fd78a33ef332da112131d8aea0362223fb03505832d9890a7', 'Tran Thanh', 'tranthanh81@gmail.com', '0', '1447024592', '2', NULL, NULL, '+84903314672', '8c55035099834c7a1dae2ef038d7a31e801ad4610ec4f', '');

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE `vote` (
  `idVote` int(11) NOT NULL,
  `productId` int(11) DEFAULT NULL,
  `1start` varchar(45) DEFAULT NULL,
  `2start` varchar(45) DEFAULT NULL,
  `3start` varchar(45) DEFAULT NULL,
  `4start` varchar(45) DEFAULT NULL,
  `5start` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`idBanner`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`idCategories`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`idInformation`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`idOrder`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`idPartner`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`idProducts`);

--
-- Indexes for table `usergruop`
--
ALTER TABLE `usergruop`
  ADD PRIMARY KEY (`idUserGruop`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUsers`);

--
-- Indexes for table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`idVote`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `idBanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `idInformation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `idOrder` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `idPartner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `idProducts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `vote`
--
ALTER TABLE `vote`
  MODIFY `idVote` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
