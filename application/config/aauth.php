<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------
  |  Aauth Config
  | -------------------------------------------------------------------
  | A library Basic Authorization for CodeIgniter 2.x
 */


// Config variables

$config['aauth'] = array(
    'login_page' => '/login',
    // if user don't have permisssion to see the page he will be
    // redirected the page spesificed below
    'no_permission' => '/',
    //name of admin group
    'technican_group' => 'Technician',
    //name of default group, the new user is added in it
    'executive_group' => 'Service Executive',
    // public group , people who not logged in
    'manager_group' => 'Service Manager',
    // public group , people who not logged in
    'admin_group' => 'Finance Manager/Admin',

    // The table which contains users
    'users' => 'users',
    // the group table
    'groups' => 'groups',
    // 
    'user_to_group' => 'user_group',
    // permitions
    'perms' => '',
    // perms to group
    'perm_to_group' => '',
    // perms to group
    'perm_to_user' => '',
    // pm table
    'pms' => 'aauth_pms',
    // system variables
    'system_variables' => '',
    // user variables
    'user_variables' => '',

    // remember time
    'remember' => ' +3 days',

    // pasword maximum char long (min is 4)
    'max' => '',

    // non alphanumeric characters that are allowed in a name
    'valid_chars' => array(' ', '\''),

    // ddos protection,
    //if it is true, the user will be banned temporary when he exceed the login 'try'
    'ddos_protection' => true,

    'recaptcha_active' => false, 
    'recaptcha_login_attempts' => 4,
    'recaptcha_siteKey' => '', 
    'recaptcha_secret' => '', 

    // login attempts time interval
    // default 20 times in one hour
    'max_login_attempt' => 10,

    // to register email verifitaion need? true / false
    'verification' => false,

    // system email.
    'email' => 'service.bestudio@gmail.com',
    'name' => 'HoaShop Service',

    'config_email' => array(
            'protocol' => "smtp",
            'smtp_host' => "ssl://smtp.googlemail.com",
            'smtp_port' => "465",
            // 'smtp_port' => "587",
            'smtp_user' => "service.bestudio@gmail.com",
            // 'smtp_user' => "postmaster@sandboxc294d3897aa64b46ba76dce450dedc22.mailgun.org",
            'smtp_pass' => "83560589",
            // 'smtp_pass' => "123456",
            'charset' => "utf-8",
            'mailtype' => "html",
            'newline' => "\r\n",
    ),
    
);


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */
