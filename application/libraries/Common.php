<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Common{
	public $CI;

	public $pagging = array(
		"current_page"=>0,
		"total_rows"=>0,
		"rows_per_page"=>5,
		);

	public $import = array(
		"url"=>array(
			"js"=>array(),
			"css"=>array(),
			), 
		"lib"=>array(
			"responsive-tables"=>FALSE,
			"fullcalendar"=>FALSE,
			"datatables"=>FALSE,
			"moment"=>FALSE,
			),
		"footer" => TRUE,
		);

	public $_breadcrumb = null;
	function __construct() {
	// get main CI object
		$this->CI = & get_instance();

		$this->CI->load->helper(array('url','form'));
		$this->CI->load->library(array("common","user_agent","session"));

		// Make breadcrumb
		$url = preg_replace('/[0-9\-]*/', '', $this->CI->uri->uri_string());
		$url = str_replace("_", " ", $url);
		$url_array = array_filter(explode("/", $url));
		$temp = "";
		$count = 0;
		foreach ($url_array as $key) {
			$temp = $temp."/".$key;
			if($count != count($url_array)-1) {
				$this->_breadcrumb[ucfirst($key)] = site_url(str_replace(" ", "_", $temp));
			} else {
				$this->_breadcrumb[ucfirst($key)] = "";
			}
			$count++;
		}
		if($this->CI->agent->referrer() !== current_url())
			$this->CI->session->set_userdata("back_url", $this->CI->agent->referrer());
	}

/**
    * View
    * <p>Using to load common view with topbar and left menu</p>
    * @param string content_view
    * @param array data have title and other data to display at view
    */
public function view($content_view, $data) {
	$data["import"] = $this->import;
	$data["breadcrumb"] = $this->_breadcrumb;
	$this->CI->load->view("design/header", $data);
	$this->CI->load->view($content_view, $data);
	$this->CI->load->view("design/footer", $data);
}
	public function view_front_end($content_view, $data){
		$data["import"] = $this->import;
		$this->CI->load->view("design/header_front_end", $data);
		$this->CI->load->view($content_view, $data);
		$this->CI->load->view("design/footer_front_end", $data);
	}
/**
    * List group user 
    * <p>Using to load common view with dropbox</p>
    */
public function get_groupuser(){
	$this->CI->db->select('*');
	$query = $this->CI->db->get('UserGruop');
	return $query->result_array();
}

/* Pagging Users List*/
public function userspagging($condition= array(),$limit = FALSE, $offset = FALSE) {
	//get du lieu can pagging
	$this->CI->db->select('users.idUsers,
		users.fullName,
		users.groupID,
		users.email,
		users.created,
		users.status,
		users.phoneNumber')
	->from("users")
	->join("UserGruop as groups", "groups.idUserGruop = users.groupID");

	// dieu kien search
	if(!empty($condition['search_key'])) {
		$this->CI->db->like('users.fullName', $condition['search_key']);
		$this->CI->db->or_like('users.email', $condition['search_key']);
		$this->CI->db->order_by('users.idUsers', 'asc');
	}

	//compile ma ko run select
	$count = $this->CI->db->_compile_select();

	// limit cho pagging
	if ($limit) {
		if ($offset == FALSE){
			$this->CI->db->limit($limit);
		}else{
			$this->CI->db->limit($limit, $offset);
		}
	}

	$query = $this->CI->db->get();
	$result["datas"] = $query->result_array();
	$query1=$this->CI->db->select("b.*", FALSE)->from("( $count ) as b ");
	$result["total_rows"] = $this->CI->db->count_all_results();  

	return $result;
}
	/* Pagging Users List*/
	public function eventspagging($condition= array(),$limit = FALSE, $offset = FALSE) {
		//get du lieu can pagging
		$this->CI->db->select('information.idInformation,
		information.title,
		information.author,
		information.description,
		information.image,
		information.created,
		information.refLink,
		information.alias,
		information.type')
		->from("information") -> where('information.type = 1') -> order_by('information.created', 'DESC');

		// dieu kien search
		if(!empty($condition['search_key'])) {
			$this->CI->db->like('information.title', $condition['search_key']);
			#$this->CI->db->or_like('Users.email', $condition['search_key']);
			# $this->CI->db->order_by('Information.created', 'ASC');
		}

		//compile ma ko run select
		$count = $this->CI->db->_compile_select();

		// limit cho pagging
		if ($limit) {
			if ($offset == FALSE){
				$this->CI->db->limit($limit);
			}else{
				$this->CI->db->limit($limit, $offset);
			}
		}

		$query = $this->CI->db->get();
		$result["datas"] = $query->result_array();
		$query1=$this->CI->db->select("b.*", FALSE)->from("( $count ) as b ");
		$result["total_rows"] = $this->CI->db->count_all_results();

		return $result;
	}

	/* Pagging banners List*/
	public function bannerspagging($condition= array(),$limit = FALSE, $offset = FALSE) {
		//get du lieu can pagging
		$this->CI->db->select('banner.idBanner,
		banner.title,
		banner.position,
		banner.image,
		banner.link,
		banner.startDate,
		banner.endDate,
		banner.expiredDate')
			->from("banner") -> order_by('banner.endDate', 'DESC');

		// dieu kien search
		if(!empty($condition['search_key'])) {
			$this->CI->db->like('banner.title', $condition['search_key']);
			#$this->CI->db->or_like('Users.email', $condition['search_key']);
			# $this->CI->db->order_by('Information.created', 'ASC');
		}

		//compile ma ko run select
		$count = $this->CI->db->_compile_select();

		// limit cho pagging
		if ($limit) {
			if ($offset == FALSE){
				$this->CI->db->limit($limit);
			}else{
				$this->CI->db->limit($limit, $offset);
			}
		}

		$query = $this->CI->db->get();
		$result["datas"] = $query->result_array();
		$query1=$this->CI->db->select("b.*", FALSE)->from("( $count ) as b ");
		$result["total_rows"] = $this->CI->db->count_all_results();

		return $result;
	}

	/* Pagging banners List*/
	public function partnerpagging($condition= array(),$limit = FALSE, $offset = FALSE) {
		//get du lieu can pagging
		$this->CI->db->select('partner.idPartner,
		partner.name,
		partner.created,
		partner.updateAt,
		partner.image,
		partner.link')
				->from("partner") -> order_by('partner.created', 'DESC');

		// dieu kien search
		if(!empty($condition['search_key'])) {
			$this->CI->db->like('partner.name', $condition['search_key']);
			#$this->CI->db->or_like('Users.email', $condition['search_key']);
			# $this->CI->db->order_by('Information.created', 'ASC');
		}

		//compile ma ko run select
		$count = $this->CI->db->_compile_select();

		// limit cho pagging
		if ($limit) {
			if ($offset == FALSE){
				$this->CI->db->limit($limit);
			}else{
				$this->CI->db->limit($limit, $offset);
			}
		}

		$query = $this->CI->db->get();
		$result["datas"] = $query->result_array();
		$query1=$this->CI->db->select("b.*", FALSE)->from("( $count ) as b ");
		$result["total_rows"] = $this->CI->db->count_all_results();

		return $result;
	}

	/* Pagging categories List*/
	public function categoriespagging($condition= array(),$limit = FALSE, $offset = FALSE) {
		//get du lieu can pagging
		$this->CI->db->select('categories.idCategories,
		categories.categoryName')
			->from("categories");

		// dieu kien search
		if(!empty($condition['search_key'])) {
			$this->CI->db->like('categories.idCategories', $condition['search_key']);
			$this->CI->db->or_like('categories.categoryName', $condition['search_key']);
		}

		//compile ma ko run select
		$count = $this->CI->db->_compile_select();

		// limit cho pagging
		if ($limit) {
			if ($offset == FALSE){
				$this->CI->db->limit($limit);
			}else{
				$this->CI->db->limit($limit, $offset);
			}
		}

		$query = $this->CI->db->get();
		$result["datas"] = $query->result_array();
		$query1=$this->CI->db->select("b.*", FALSE)->from("( $count ) as b ");
		$result["total_rows"] = $this->CI->db->count_all_results();

		return $result;
	}

	/* Pagging Products List*/
	public function productspagging($condition= array(),$limit = FALSE, $offset = FALSE) {
		//get du lieu can pagging
		$this->CI->db->select('products.idProducts,
		products.productName,
		products.price,
		products.created,
		products.description,
		products.image,
		products.detail,
		products.discount',FALSE)
			->from('products');

		if(!empty($condition['categories_id'])){
		$this->CI->db->join('categories','products.categoryId=categories.idCategories');
		$this->CI->db->where('categories.idCategories',$condition['categories_id']);
		}
		// dieu kien search
		if(!empty($condition['search_key'])) {
			$this->CI->db->like('products.productName', $condition['search_key']);
			$this->CI->db->or_like('products.price', $condition['search_key']);
			$this->CI->db->or_like('products.idProducts', $condition['search_key']);
		}

		//compile ma ko run select
		$count = $this->CI->db->_compile_select();

		// limit cho pagging
		if ($limit) {
			if ($offset == FALSE){
				$this->CI->db->limit($limit);
			}else{
				$this->CI->db->limit($limit, $offset);
			}
		}

		$query = $this->CI->db->get();
		$result["datas"] = $query->result_array();
		$query1=$this->CI->db->select("b.*", FALSE)->from("( $count ) as b ");
		$result["total_rows"] = $this->CI->db->count_all_results();

		return $result;
	}
}
?>