<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products_model extends CI_Model {
	public $CI;
	public function __construct()
	{
    // Call the CI_Model constructor
		parent::__construct();
		$this->CI = & get_instance();
		$this->load->library("Aauth");
		$this->load->helper('date');
	}

	public function insert_new_product($productinfo)
	{
		$datainsert= array(
			'productName' => $productinfo['productname'],
			'categoryId' => $productinfo['categoryid'],
			'price' => $productinfo['price'],
			'created' =>now(),
			'alias' => $productinfo['alias'],
			'description' => $productinfo['description'],
			'image' => $productinfo['image'],
			'detail' => $productinfo['detail'],
			'discount' => $productinfo['discount']
			);
		$this->db->insert('products',$datainsert);
	}

	public function delete_product_by_id($productid){
		$this->db->where('idProducts',$productid);
		$this->db->delete('products');
	}

	public function update_product_by_id($productid,$data){
		$dataupdate = array(
			'productName' => $data["productname"],
			'categoryId' => $data['categoryid'],
			'price' => $data['price'],
			'created' =>now(),
			'alias' => $data['alias'],
			'description' => $data['description'],
			'image' => $data['image'],
			'detail' => $data['detail'],
			'discount' => $data['discount']

		);
		$this->db->where('idProducts',$productid);
		$this->db->update('products', $dataupdate);
	}

	public function get_product_by_id($productid){
		$this->db->where('idProducts',$productid);
		$query=$this->db->get('products');
		return $query->result_array();
	}

}
?>