<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model {
	public $CI;
	public function __construct()
	{
    // Call the CI_Model constructor
		parent::__construct();
		$this->CI = & get_instance();
		$this->load->library("Aauth");
		$this->load->helper('date');
	}

	public function check_user_valid($email, $pass){
		$query = $this->db->query("SELECT * FROM `users` where `email` = '".$email."' and `password` = '".$pass."'");
		if ($query->num_rows() > 0)
		{
			return true;
		}
		return false;
	}

	public function insert_new_account($userinfo)
	{
		$user_id = $this->CI->db->insert_id();
		$datainsert= array(
			'password' => $this->aauth->hash_password($userinfo["password"], $user_id),
			'fullName' => $userinfo["fullname"],
			'email'	=> $userinfo["email"],
			'status'=> 0,
			'created'=> now(),
			'groupID'=> $userinfo['groupid'],
			'verification_code'=> $this->aauth->hash_password($userinfo["email"],$user_id),
			'phoneNumber' => $userinfo['phonenumber']
			);
		$this->db->insert('users',$datainsert);
	}

	public function delete_user_by_id($userid){
		$this->db->where('idUsers',$userid);
		$this->db->delete('users');
	}

	public function get_user_by_id($userid){
		$this->db->where('idUsers',$userid);
		$query=$this->db->get('users');
		return $query->result_array();
	}

	public function update_user_by_id($userid,$data){
		$dataupdate = array(
			'fullName' => $data["fullname"],
			'groupID' =>$data["role"],
			'phoneNumber' =>$data["phonenumber"]
		);
		$this->db->where('idUsers',$userid);
		$this->db->update('users', $dataupdate);
	}

}
?>