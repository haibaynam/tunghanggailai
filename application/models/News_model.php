<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News_model extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result();
    }

    public function get_hot_news(){
        $query = $this->db->query('SELECT * FROM `information` where `type` = 1 order by `hit` DESC Limit 7');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_latest_news(){
        $query = $this->db->query('SELECT * FROM `information` where `type` = 1 order by `created` DESC Limit 3');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_products(){
        $query = $this->db->query('SELECT * FROM `products` order by `created` DESC');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_about_us(){
        $query = $this->db->query('SELECT * FROM `information` where `idInformation` = 1');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_banner_with_position($position){
        $query = $this->db->query('SELECT * FROM `banner` where `position` = '."'".$position."'");
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_detail_news($alias){
        $query = $this->db->query('SELECT * FROM `information` where `alias` = '."'".$alias."'");
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_detail_products($alias){
        $query = $this->db->query('SELECT * FROM `products` where `alias` = '."'".$alias."'");
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
}