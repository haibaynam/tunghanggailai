<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categories_model extends CI_Model {
	public $CI;
	public function __construct()
	{
    // Call the CI_Model constructor
		parent::__construct();
		$this->CI = & get_instance();
		$this->load->library("Aauth");
		$this->load->helper('date');
	}

	public function insert_new_category($cateinfo)
	{
		$datainsert= array(
			'categoryName' => $cateinfo['categoryname']
			);
		$this->db->insert('categories',$datainsert);
	}

	public function delete_category_by_id($cateid){
		$this->db->where('idCategories',$cateid);
		$this->db->delete('categories');
	}

	public function update_cate_by_id($cateid,$data){
		$dataupdate = array(
			'categoryName' => $data["categoryname"],
		);
		$this->db->where('idCategories',$cateid);
		$this->db->update('categories', $dataupdate);
	}

	public function get_categories(){
		$query=$this->db->get('categories');
		return $query->result_array();
	}
}
?>