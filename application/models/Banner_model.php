<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Banner_model extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result();
    }

    public function get_entries_with_type($type)
    {
        $query = $this->db->query('SELECT * FROM `information` where `type` = '.$type.' order by `created` DESC');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_entries_with_id($id)
    {
        $query = $this->db->query('SELECT * FROM `banner` where `idBanner` = '.$id.'');
        if ($query->num_rows() > 0)
            return $query->result();
    }

    public function insert_entry($data)
    {
        $this->db->insert('banner', $data);
    }

    public function update_entry($data, $id)
    {
        $this->db->update('banner', $data, array('idBanner' => $id));
        return true;
    }

    public function delete_entry($id)
    {
        $this->db->delete('banner', array('idBanner' => $id));
    }

    public function search_entry($value)
    {
        $query = $this->db->query("SELECT * FROM `information` where `type` = 1 AND `title` like '%$value%'");
        if ($query->num_rows() > 0)
            return $query->result();
    }
}