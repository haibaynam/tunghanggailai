<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Statistics_model extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result();
    }

    public function get_recently_added_products(){
        $query = $this->db->query('SELECT * FROM `products` order by `created` DESC Limit 3');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_new_order(){
        $query = $this->db->query('SELECT * FROM `order` order by `created` DESC Limit 7');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_new_users(){
        $query = $this->db->query('SELECT * FROM `users` order by `created` DESC Limit 8');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function count_users_registation(){
        $query = $this->db->query('SELECT COUNT(`idUsers`) AS `all_users` FROM `users`');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function count_new_orders(){
        $query = $this->db->query('SELECT COUNT(`idOrder`) AS `new_order` FROM `order`');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function count_all_product(){
        $query = $this->db->query('SELECT COUNT(`idProducts`) AS `all_products` FROM `products`');
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

}