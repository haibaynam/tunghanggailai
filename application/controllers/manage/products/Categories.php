<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Dashboard
 *
 */
class Categories extends CI_Controller{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth"));
        $this->load->model('Categories_model');
    }    
    
    public function index() {        
        $data["title"] = "Users Management";
        $search_condition['search_key']=$this->input->post('user_search');
        $this->common->pagging["current_page"] = $this->input->post("current_page");
        $categorie = $this->common->categoriespagging($search_condition,$this->common->pagging["rows_per_page"],
        $this->common->pagging["rows_per_page"] * $this->common->pagging["current_page"]);
        $data["categories_list"] = $categorie["datas"];
        $this->common->pagging["total_rows"] = $categorie["total_rows"];
        $this->common->view('manage/categories/categories_main',$data);

    }

    public function create_new_categories(){
        $this->form_validation->set_rules("categoryname", "Category Name", "trim|required|is_unique[Categories.categoryName]");
        if($this->form_validation->run()){
            $data = $this->input->post();
            $this->Categories_model->insert_new_category($data);
            $datas["status"]="OK";
            $this->session->set_flashdata('message', "{text: 'New Category has been created', type: 'success'}");
            echo json_encode($datas);
        }else{
            $datas["status"]="ERROR";
            $datas["message"] = validation_errors();
            echo json_encode($datas);
        }
    }

    public function removecategory($cateid){
        $this->Categories_model->delete_category_by_id($cateid);
        return redirect(base_url($this->config->item("index_page")."/manage/products/Categories"));
    }

    public function editcategory(){
        $this->form_validation->set_rules("categoryname","Category Name", "trim|required|is_unique[Categories.categoryName]");
        if($this->form_validation->run()){
            $dataedit = $this->input->post();
            $this->Categories_model->update_cate_by_id($dataedit['categoryid'],$dataedit);
            $datas["status"]="OK";
            $this->session->set_flashdata('message', "{text: 'Categories has been edited', type: 'success'}");
            echo json_encode($datas);
        }else{
            $datas["status"]="ERROR";
            $datas["message"] = validation_errors();
            echo json_encode($datas);
        }
    }

}
