<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Dashboard
 *
 */
class Products extends CI_Controller{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth","session"));
        $this->load->model(array('Products_model', 'Categories_model', 'News_model'));
    }    
    
    public function index() {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data["title"] = "Products Management";
        $data["categories_list"] = $this->Categories_model->get_categories();
        $search_condition['search_key']=$this->input->post('user_search');
        $search_condition['categories_id']= $this->input->post('idCategories');
        $this->common->pagging["current_page"] = $this->input->post("current_page");
        $product = $this->common->productspagging($search_condition,$this->common->pagging["rows_per_page"],
        $this->common->pagging["rows_per_page"] * $this->common->pagging["current_page"]);
        $data["products_list"] = $product["datas"];
        $this->common->pagging["total_rows"] = $product["total_rows"];
        $this->common->view('manage/products/products_main',$data);

    }

    public function load_product_by_categories($categoryid){
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $search_condition['search_key']=$this->input->post('user_search');
        $search_condition['categories_id']= $categoryid;
        $this->common->pagging["current_page"] = $this->input->post("current_page");
        $products = $this->common->productspagging($search_condition,$this->common->pagging["rows_per_page"],
        $this->common->pagging["rows_per_page"] * $this->common->pagging["current_page"]);
        $data["products_list"] = $products["datas"];
        $this->common->pagging["total_rows"] = $products["total_rows"];
        $this->load->view('manage/products/products_by_categories',$data);
    }

    public function create_new_product(){
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data["title"]="Add New Product";
        $data["categories_list"] = $this->Categories_model->get_categories();
        if($this->input->post('addproduct')){
        $this->form_validation->set_rules("image", "Image", "trim|required");
        $this->form_validation->set_rules("productname", "Product Name", "trim|required");
        $this->form_validation->set_rules("categoriesselect", "Categories", "trim|required");
        $this->form_validation->set_rules("productprice", "Price", "trim|required");
        $this->form_validation->set_rules("productdiscount", "Discount", "trim|numeric|max_length[100]|min_length[0]");
        
        if($this->form_validation->run()){
            $insertdata = array(
                'image' => $this->input->post('image'),
                'productname' => $this->input->post('productname'),
                'categoryid'=>$this->input->post('categoriesselect'),
                'alias' => $this->input->post('alias'),
                'price' => $this->input->post('productprice'),
                'discount' => $this->input->post('productdiscount'),
                'description' => $this->input->post('description'),
                'detail' => $this->input->post('detail')
                );
            $this->Products_model->insert_new_product($insertdata);
            $this->session->set_flashdata('message', "{text: 'New Product has been created', type: 'success'}");
        }
        }
        $data["message"] = validation_errors();
        $this->common->view('manage/products/add_new_product',$data);
    }

    public function removeProduct($productid){
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $this->Products_model->delete_product_by_id($productid);
        return redirect(base_url($this->config->item("index_page")."/manage/products/Products"));
    }

    public function editProduct($productid){
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data["title"]="Edit Product";
        $data["categories_list"] = $this->Categories_model->get_categories();
        $data['old']=$this->Products_model->get_product_by_id($productid);

        if($this->input->post('editproduct')){
        $this->form_validation->set_rules("image", "Image", "trim|required");
        $this->form_validation->set_rules("productname", "Product Name", "trim|required");
        $this->form_validation->set_rules("categoriesselect", "Categories", "trim|required");
        $this->form_validation->set_rules("productprice", "Price", "trim|required");
        $this->form_validation->set_rules("productdiscount", "Discount", "trim|numeric|max_length[100]|min_length[0]");
        if($this->form_validation->run()){
            $insertdata = array(
                'image' => $this->input->post('image'),
                'productname' => $this->input->post('productname'),
                'categoryid'=>$this->input->post('categoriesselect'),
                'alias' => $this->input->post('alias'),
                'price' => $this->input->post('productprice'),
                'discount' => $this->input->post('productdiscount'),
                'description' => $this->input->post('description'),
                'detail' => $this->input->post('detail')
                );
            $this->Products_model->update_product_by_id($productid,$insertdata);
            $this->session->set_flashdata('message', "{text: 'Edit Successfull', type: 'success'}");
            redirect(base_url($this->config->item("index_page")."/manage/products/Products"));
        }
    }
        $data["message"] = validation_errors();
        $this->common->view('manage/products/edit_product',$data);
    }

    public function detail($alias=''){
        $data["detail"] = $this->News_model->get_detail_products($alias);
        $data["title"] = '';
        foreach ($data["detail"] as $de){
            $data["title"] = $de->productName;
        }
        $data["hot"] = $this->News_model->get_hot_news();
        $data["banner_right_top_detail"] = $this->News_model->get_banner_with_position(12);
        $this->common->view_front_end('products/detail',$data);
    }
}
