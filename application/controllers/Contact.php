<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth"));
        $this->load->helper("date");
        $this->load->model("News_model");
    }

    public function index()
    {
        $data['title'] = "Liên hệ";
        $this->common->view_front_end('contact/index',$data);
    }
}
