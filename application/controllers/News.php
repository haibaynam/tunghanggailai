<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth"));
        $this->load->helper("date");
        $this->load->model("News_model");
    }

    public function index()
    {
        $data['title'] = "Tin tức - Sự kiện";
        $search_condition['search_key']=$this->input->post('search_key');
        $this->common->pagging["current_page"] = $this->input->post("current_page");
        $news = $this->common->eventspagging($search_condition,$this->common->pagging["rows_per_page"],
            $this->common->pagging["rows_per_page"] * $this->common->pagging["current_page"]);
        $data["news"] = $news["datas"];
        $data["hot"] = $this->News_model->get_hot_news();
        $data["banner_right_top"] = $this->News_model->get_banner_with_position(9);
        $data["banner_right_bottom_news"] = $this->News_model->get_banner_with_position(11);

        $this->common->pagging["total_rows"] = $news["total_rows"];
        $this->common->view_front_end('news/index',$data);
    }

    public function detail($alias=''){
        $data["detail"] = $this->News_model->get_detail_news($alias);
        $data["title"] = '';
        foreach ($data["detail"] as $de){
            $data["title"] = $de->title;
        }
        $data["hot"] = $this->News_model->get_hot_news();
        $data["banner_right_top_detail"] = $this->News_model->get_banner_with_position(12);
        $this->common->view_front_end('news/detail',$data);
    }
}
