<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Dashboard
 *
 */
class Banner extends CI_Controller{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth", "session"));
        $this->load->model('Banner_model');
        $this->load->helper('date');
    }

    public function index() {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data["title"] = "Banner manage page";
        $search_condition['search_key']=$this->input->post('search_key');
        $this->common->pagging["current_page"] = $this->input->post("current_page");
        $banners = $this->common->bannerspagging($search_condition,$this->common->pagging["rows_per_page"],
            $this->common->pagging["rows_per_page"] * $this->common->pagging["current_page"]);
        $data["banner"] = $banners["datas"];
        $this->common->pagging["total_rows"] = $banners["total_rows"];
        $this->common->view('banner/index',$data);
    }

    public function new_banner() {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data['title'] = 'Banner';
        if($_POST) {
            $this->form_validation->set_rules("title", "[Title]", "trim|required");
            $this->form_validation->set_rules("position", "[Position]", "trim|required");
            $this->form_validation->set_rules("link", "[Link]", "trim|required");
            $this->form_validation->set_rules("startDate", "[Start Day]", "trim|required");
            $this->form_validation->set_rules("endDate", "[End Day]", "trim|required");
            $this->form_validation->set_rules("image", "[Image]", "trim|required");

            if($this->form_validation->run()){
                $arrayDetail = $this->input->post();
                $this->Banner_model->insert_entry($arrayDetail);
                $this->session->set_flashdata('message', "{text: 'Banner has been created', type: 'success'}");
                redirect('/Banner/', 'refresh');
            }
            else{
                $data["status"]="ERROR";
                $data["message"] = validation_errors();
            }
        }
        $this->common->view('banner/new-banner',$data);
    }

    public function update_banner($id) {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data['title'] = 'Update Banner';
        if($_POST) {
            $this->form_validation->set_rules("title", "[Title]", "trim|required");
            $this->form_validation->set_rules("position", "[Position]", "trim|required");
            $this->form_validation->set_rules("link", "[Link]", "trim|required");
            $this->form_validation->set_rules("startDate", "[Start Day]", "trim|required");
            $this->form_validation->set_rules("endDate", "[End Day]", "trim|required");
            $this->form_validation->set_rules("image", "[Image]", "trim|required");

            if($this->form_validation->run()){
                $arrayDetail = $this->input->post();
                $this->Banner_model->update_entry($arrayDetail,$id);
                $this->session->set_flashdata('message', "{text: 'Banner has been edited', type: 'success'}");
                redirect('/Banner/', 'refresh');
            }
            else{

                $data["status"]="ERROR";
                $data["message"] = validation_errors();
            }
        }
        $data['detail'] = $this->Banner_model->get_entries_with_id($id);
        $this->common->view('banner/update-banner',$data);
    }

    public function delete_banner($id)
    {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $this->Banner_model->delete_entry($id);
        redirect('/Banner/', 'refresh');
    }
}
