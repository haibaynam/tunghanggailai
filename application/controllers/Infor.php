<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Dashboard
 *
 */
class Infor extends CI_Controller{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth", "session"));
        $this->load->model('Information');
        $this->load->helper('date');
    }    
    
    public function index() {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data["title"] = "Information page";        
        $data['infor'] = $this->Information->get_entries_with_type(0);
        # $data['event'] = $this->Information->get_entries_with_type(1);
        $search_condition['search_key']=$this->input->post('search_key');
        $this->common->pagging["current_page"] = $this->input->post("current_page");
        $events = $this->common->eventspagging($search_condition,$this->common->pagging["rows_per_page"],
                                            $this->common->pagging["rows_per_page"] * $this->common->pagging["current_page"]);
        $data["event"] = $events["datas"];
        $this->common->pagging["total_rows"] = $events["total_rows"];
        $this->common->view('information/index',$data);
    }

    public function update($id) {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data['title'] = 'Update Information';
        $this->form_validation->set_rules("title", "[Title]", "trim|required");
        $this->form_validation->set_rules("alias", "[Alias]", "trim|required");
        $this->form_validation->set_rules("refLink", "[Referent Link]", "trim|required");
        $this->form_validation->set_rules("type", "[Type]", "trim|required");
        $this->form_validation->set_rules("description", "[Description]", "trim|required");

        if($_POST) {
            if($this->form_validation->run()){
                $title = $this->input->post('title');
                $alias = $this->input->post('alias');
                $refLink = $this->input->post('refLink');
                $type = $this->input->post('type');
                $description = $this->input->post('description');
                $created = now();
                $arrayDetail = array('title' => $title, 'alias' => $alias, 'refLink' => $refLink, 'type' => $type,
                    'description' => $description, 'created' => $created);
                $this->Information->update_entry($arrayDetail, $id);
                $this->session->set_flashdata('message', "{text: 'Information has been edited', type: 'success'}");
                redirect('/Infor/', 'refresh');
            }
            else{
                $data["status"]="ERROR";
                $data["message"] = validation_errors();
            }
        }
     
        $data['detail'] = $this->Information->get_entries_with_id($id);
        $this->common->view('information/update',$data);
    }

    public function new_event() {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data['title'] = 'Add New Event';
        if($_POST) {
            $this->form_validation->set_rules("title", "[Title]", "trim|required");
            $this->form_validation->set_rules("alias", "[Alias]", "trim|required");
            $this->form_validation->set_rules("refLink", "[Referent Link]", "trim|required");
            $this->form_validation->set_rules("type", "[Type]", "trim|required");
            $this->form_validation->set_rules("description", "[Description]", "trim|required");
            $this->form_validation->set_rules("image", "[Image]", "trim|required");

            if($this->form_validation->run()){
                $title = $this->input->post('title');
                $alias = $this->input->post('alias');
                $refLink = $this->input->post('refLink');
                $type = $this->input->post('type');
                $description = $this->input->post('description');
                $image = $this->input->post('image');
                $created = now();
                $arrayDetail = array('title' => $title, 'alias' => $alias, 'refLink' => $refLink, 'type' => $type,
                    'description' => $description, 'created' => $created, 'image' => $image, 'author' => "Admin");
                $this->Information->insert_entry($arrayDetail);
                $this->session->set_flashdata('message', "{text: 'Event has been created', type: 'success'}");
                redirect('/Infor/', 'refresh');
            }
            else{
                $data["status"]="ERROR";
                $data["message"] = validation_errors();
            }
        }
        $this->common->view('information/new-event',$data);
    }

    public function update_event($id) {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data['title'] = 'Update Event';
        if($_POST) {
            $this->form_validation->set_rules("title", "[Title]", "trim|required");
            $this->form_validation->set_rules("alias", "[Alias]", "trim|required");
            $this->form_validation->set_rules("refLink", "[Referent Link]", "trim|required");
            $this->form_validation->set_rules("type", "[Type]", "trim|required");
            $this->form_validation->set_rules("description", "[Description]", "trim|required");
            $this->form_validation->set_rules("image", "[Image]", "trim|required");

            if($this->form_validation->run()){
                $title = $this->input->post('title');
                $alias = $this->input->post('alias');
                $refLink = $this->input->post('refLink');
                $type = $this->input->post('type');
                $description = $this->input->post('description');
                $image = $this->input->post('image');
                $created = now();
                $arrayDetail = array('title' => $title, 'alias' => $alias, 'refLink' => $refLink, 'type' => $type,
                    'description' => $description, 'created' => $created, 'image' => $image);
                $this->Information->update_entry($arrayDetail,$id);
                $this->session->set_flashdata('message', "{text: 'Event has been edited', type: 'success'}");
                redirect('/Infor/', 'refresh');
            }
            else{

                $data["status"]="ERROR";
                $data["message"] = validation_errors();
            }
        }
        $data['detail'] = $this->Information->get_entries_with_id($id);
        $this->common->view('information/update-event',$data);
    }

    public function delete_event($id)
    {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $this->Information->delete_entry($id);
        redirect('/Infor/', 'refresh');
    }

    public function search_event($value)
    {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data['data_search'] = $this->Information->search_entry($value);
        $this->load->view('information/ajax/data_search', $data);
    }
}
