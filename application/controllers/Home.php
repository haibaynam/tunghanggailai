<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth"));
        $this->load->model("News_model");
        $this->load->helper("date");
    }

    public function index()
    {
        $data['title'] = "Trang chu";
        $data['latest'] = $this->News_model->get_latest_news();
        $data['about-us'] = $this->News_model->get_about_us();
        $data['product'] = $this->News_model->get_products();
        $about_us = '';
        foreach($data['about-us'] as $about){
             $about_us = substr($about->description,0,800);
        }
        $data['about_us'] = $about_us;
        $this->common->view_front_end('home/index',$data);
    }
}
