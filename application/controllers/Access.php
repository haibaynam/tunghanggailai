<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation', 'session');
        $this->load->model('Users_model');
    }  
    
    public function index()
    {
         $this->load->view('access/login');
    }
    
    public function validate() {
        $this->form_validation->set_rules('usermail', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('userpass', 'Password', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('access/login');
        }else{
            $email = $this->input->post('usermail');
            $pass = $this->input->post('userpass');

//            7e43744b99d22ea9c579629b34f827c6
            if ($this->Users_model->check_user_valid($email, md5($pass)))
                $this->session->set_userdata('login', 1);
                redirect('Infor');
        }
    }
}
