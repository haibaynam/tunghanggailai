<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Dashboard
 *
 */
class Dashboard extends CI_Controller{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->load->library('common',"session");
        $this->load->model('Statistics_model');
        $this->load->helper('date');
    }    
    
    public function index() {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data["title"] = "Dash - Board";
        $data["new_products"] = $this->Statistics_model->get_recently_added_products();
        $data["new_order"] = $this->Statistics_model->get_new_order();
        $data["count_users"] = $this->Statistics_model->count_users_registation();
        $data["count_orders"] = $this->Statistics_model->count_new_orders();
        $data["count_products"] = $this->Statistics_model->count_all_product();
        $data["new_users"] = $this->Statistics_model->get_new_users();
        $data["sale"] = "[
              {y: '2015 Q1', item1: 100},
              {y: '2015 Q2', item1: 330},
            ]";
        $this->common->view('dashboard/admin',$data);
    }
}
