<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Introduction extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth"));
        $this->load->model("News_model");
        $this->load->helper("date");
    }

    public function index()
    {
        $data['title'] = "Giới thiệu";
        $data['about_us'] = $this->News_model->get_about_us();
        $this->common->view_front_end('introduction/index',$data);
    }
}
