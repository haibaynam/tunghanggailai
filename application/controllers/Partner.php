<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Dashboard
 *
 */
class Partner extends CI_Controller{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("common", "form_validation", "Aauth", "session"));
        $this->load->model('Partner_model');
        $this->load->helper('date');
    }

    public function index() {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data["title"] = "Partner manage page";
        $search_condition['search_key']=$this->input->post('search_key');
        $this->common->pagging["current_page"] = $this->input->post("current_page");
        $partner = $this->common->partnerpagging($search_condition,$this->common->pagging["rows_per_page"],
            $this->common->pagging["rows_per_page"] * $this->common->pagging["current_page"]);
        $data["partner"] = $partner["datas"];
        $this->common->pagging["total_rows"] = $partner["total_rows"];
        $this->common->view('partner/index',$data);
    }

    public function new_partner() {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data['title'] = 'Partner';
        if($_POST) {
            $this->form_validation->set_rules("name", "Title", "trim|required");
            $this->form_validation->set_rules("link", "Link", "trim|required");
            $this->form_validation->set_rules("image", "Image", "trim|required");

            if($this->form_validation->run()){
                $arrayDetail = $this->input->post();
                $created = now();
                $arrayDetail["created"] = $created;
                $this->Partner_model->insert_entry($arrayDetail);
                $this->session->set_flashdata('message', "{text: 'Partner has been created', type: 'success'}");
                redirect('/Partner/', 'refresh');
            }
            else{
                $data["status"]="ERROR";
                $data["message"] = validation_errors();
            }
        }
        $this->common->view('partner/new-partner',$data);
    }

    public function update_partner($id) {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $data['title'] = 'Update Partner';
        if($_POST) {
            $this->form_validation->set_rules("name", "Title", "trim|required");
            $this->form_validation->set_rules("link", "Link", "trim|required");
            $this->form_validation->set_rules("image", "Image", "trim|required");

            if($this->form_validation->run()){
                $arrayDetail = $this->input->post();
                $updateAt = now();
                $arrayDetail["updateAt"] = $updateAt;
                $this->Partner_model->update_entry($arrayDetail,$id);
                $this->session->set_flashdata('message', "{text: 'Partner has been edited', type: 'success'}");
                redirect('/Partner/', 'refresh');
            }
            else{

                $data["status"]="ERROR";
                $data["message"] = validation_errors();
            }
        }
        $data['detail'] = $this->Partner_model->get_entries_with_id($id);
        $this->common->view('partner/update-partner',$data);
    }

    public function delete_partner($id)
    {
        if ($this->session->userdata('login') != 1)
            redirect('Access');

        $this->Partner_model->delete_entry($id);
        redirect('/Partner/', 'refresh');
    }
}
