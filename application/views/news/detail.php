<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="head-title">
<div class="container">
    <div class="row">
        <h2 class="page-title">
            <?php
                echo $title;
            ?>
        </h2>
    </div>
</div>
</div>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="content-area col-md-8" id="primary">
                <div class="site-content" id="content">
                    <div class="post format-image hentry">
                        <?php
                            if(!empty($detail)){
                                foreach($detail as $detail){
                                    ?>
                        <header class="entry-header">
                            <div class="entry-format">
                                <div class="entry-meta">
                                    <div class="pull-left">
                                        <span class="entry-date">
                                        <a href="#">
                                            <time class="published" datetime="<?php
                                            $now = time();
                                            echo timespan($detail->created, $now);
                                            ?>">
                                                <?php
                                                $now = time();
                                                echo timespan($detail->created, $now);
                                                ?>
                                            </time>
                                        </a>
                                        </span>
                                        <span class="author vcard">
                                        Đăng bởi <a><?php echo $detail->author;?></a>
                                        </span>
                                    </div>
                                    <div class="pull-right">

                                        <div class="social-shares">
                                            <i class="fa fa-fw fa-share-alt"></i>
                                            <a href="#">Chia sẻ</a>

                                            <div class="other-share">
                                                <a href="" class="facebook">
                                                    <i class="fa fa-fw fa-facebook"></i>
                                                    <span class="icon-title">Facebook</span>
                                                </a>
                                                <a href="" class="twitter">
                                                    <i class="fa fa-fw fa-twitter"></i>
                                                    <span class="icon-title">Twitter</span>
                                                </a>
                                                <a href="" onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="google-plus">
                                                    <i class="fa fa-fw fa-google-plus"></i>
                                                    <span class="icon-title">Google Plus</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h1 class="entry-title"><?php echo $detail->title;?></h1>
                        </header>
                        <div class="entry-content">
                            <?php echo $detail->description;?>
                        </div>
                    </div>

                    <div class="comment-outer">
                        <div class="fb-comments" data-href="<?=base_url("index.php/news/".$detail->alias.".html");?>" data-width="690" data-numposts="5"></div>
                    </div>

                </div>
            </div>
                                <?php
                                }
                            }
                        ?>

            <aside id="secondary" class="col-md-4">
                <div class="sidebar">

                    <div class="widget">
                        <!--<h3 class="widget-title">Thương hiệu</h3>-->
                        <?php
                        if (!empty($banner_right_top_detail)){
                            foreach($banner_right_top_detail as $banner){
                                ?>
                                <img src="<?php echo $banner->image;?>" />
                                <?php
                            }
                        }
                        ?>
                    </div>

                    <div class="widget post-type-widget">
                        <h3 class="widget-title">Bài viết nổi bật nhất</h3>
                        <ul>
                            <?php
                            if (!empty($hot)){
                                foreach($hot as $hot){
                                    ?>
                                    <li>
                                <span class="post-category">
                                </span>
                                        <figure class="post-thumbnail">
                                            <a href="<?=base_url("index.php/news/".$hot->alias.".html");?>"><img src="<?php echo $hot->image;?>"></a>
                                        </figure>
                                        <h2 class="post-title">
                                            <a href="<?=base_url("index.php/news/".$hot->alias.".html");?>"><?php echo $hot->title;?></a>
                                        </h2>
                                    </li>
                                    <?php
                                }
                            }
                            ?>

                        </ul>
                    </div>

                </div>
            </aside>
        </div>
    </div>
</div>