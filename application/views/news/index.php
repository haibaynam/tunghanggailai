<div class="head-title">
    <div class="container">
        <div class="row">
            <h2 class="page-title">Tin tức</h2>
        </div>
    </div>
</div>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="content-area col-md-8" id="primary">
                <div class="site-content" id="content">
                    <?php
                    if (!empty($news)){
                        foreach($news as $news){
                            ?>
                            <div class="post format-image hentry">
                                <header class="entry-header">
                                    <div class="entry-format">
                                        <div class="entry-meta">
                                            <div class="pull-left">
										<span class="cat-links">
                                            <a href="news/<?php echo $news["alias"];?>.html"><?php echo $news["title"];?></a>
										</span>
										<span class="entry-date">
											<a href="#">
                                                <time class="published" datetime="<?php
                                                $now = time();
                                                echo timespan($news["created"], $now);
                                                ?>">
                                                    <?php
                                                    $now = time();
                                                    echo timespan($news["created"], $now);
                                                    ?>
                                                </time>
                                            </a>
										</span>
										<span class="author vcard">
											Đăng bởi <a><?php echo $news["author"];?></a>
										</span>
                                            </div>
                                            <div class="pull-right">
                                                <!--<div class="comment-link">
                                                    <i class="fa fa-fw fa-comments-o"></i>
                                                    <a href="bo-tri-phong-khach-nha-ong-mang-lai-tai-loc-cho-gia-chu.html">1 bình luận</a>
                                                </div>-->
                                                <div class="social-shares">
                                                    <i class="fa fa-fw fa-share-alt"></i>
                                                    <a href="#">Chia sẻ</a>

                                                    <div class="other-share">
                                                        <a target="_blank" href="#" class="facebook">
                                                            <i class="fa fa-fw fa-facebook"></i>
                                                            <span class="icon-title">Facebook</span>
                                                        </a>
                                                        <a target="_blank" href="#" class="twitter">
                                                            <i class="fa fa-fw fa-twitter"></i>
                                                            <span class="icon-title">Twitter</span>
                                                        </a>
                                                        <a target="_blank" href="#"
                                                           onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                           class="google-plus">
                                                            <i class="fa fa-fw fa-google-plus"></i>
                                                            <span class="icon-title">Google Plus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="entry-media">
                                        <img src="<?php echo $news["image"];?>">
                                    </div>

                                   <!-- <h1 class="entry-title">
                                        <a href="<?php /*echo $news["alias"];*/?>"><?php /*echo $news["title"];*/?></a>
                                    </h1>-->
                                </header>
                                <br/>
<!--                                <div class="entry-content">-->
<!--                                    <p class="description">--><?php //echo $news["description"];?><!--</p>-->
<!--                                </div>-->
                            </div>
                    <?php
                        }
                    }
                    ?>

                </div>
                <?php $this->load->view("design/pagging", $this->common->pagging); ?>

            </div>
            <aside id="secondary" class="col-md-4">
                <div class="sidebar">
                    <div class="widget">
                        <!--<h3 class="widget-title">Thương hiệu</h3>-->
                        <?php
                        if (!empty($banner_right_top)){
                            foreach($banner_right_top as $banner){
                                ?>
                                <img src="<?php echo $banner->image;?>" />
                                <?php
                            }
                        }
                        ?>
                    </div>

                    <div id="search-2" class="widget widget_search">
                        <h3 class="widget-title">Tìm kiếm</h3>

                        <div class="searchform">
                            <form id="event" method="POST" accept-charset="utf-8">
                                <input type="text" class="txt" name="search_key" id="search-text" placeholder="Nhập từ khóa tìm kiếm">
                                <input type="submit" value="search" class="btn btn-sm">
                                <input type="hidden" id="current_page" name="current_page" value="">
                            </form>
                        </div>
                    </div>


                    <div class="widget post-type-widget">
                        <h3 class="widget-title">Bài viết nổi bật nhất</h3>
                        <ul>
                            <?php
                            if (!empty($hot)){
                                foreach($hot as $hot){
                                    ?>
                                    <li>
                                <span class="post-category">
                                </span>
                                        <figure class="post-thumbnail">
                                            <a href="<?=base_url("index.php/news/".$hot->alias.".html");?>"><img src="<?php echo $hot->image;?>"></a>
                                        </figure>
                                        <h2 class="post-title">
                                            <a href="<?=base_url("index.php/news/".$hot->alias.".html");?>"><?php echo $hot->title;?></a>
                                        </h2>
                                    </li>
                            <?php
                                }
                            }
                            ?>

                        </ul>
                    </div>


                    <div class="widget">
                        <h3 class="widget-title">Quảng cáo</h3>
                        <?php
                        if (!empty($banner_right_bottom_news)){
                            foreach($banner_right_bottom_news as $banner1){
                                ?>
                                <a href="<?php echo $banner1->link;?>" target="_blank"><img src="<?php echo $banner1->image;?>" /></a>
                                <?php
                            }
                        }
                        ?>
                    </div>

                </div>
            </aside>
        </div>
    </div>
</div>
<script type="application/javascript">
    $(".menu li").removeClass("active");
    $(".menu #news").addClass("active");
    $(".pagination>li>a").click(function() {
        if($(this).attr("page") != "")
            pagging("event", $(this).attr("page"));
    });
</script>