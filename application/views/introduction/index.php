<div class="head-title">
    <div class="container">
        <div class="row">
            <h2 class="page-title">Cà Phê Tùng Hằng Gia Lai</h2>
        </div>
    </div>
</div>
<section id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 post">
                <h2>Giới thiệu</h2>

                <div class="page">
                    <?php
                        if (!empty($about_us)){
                            foreach($about_us as $about_us){
                               echo $about_us->description;
                            }
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(".menu li").removeClass("active");
    $(".menu #introduction").addClass("active");
</script>