<!--Small box (Start box) -->
<div class="row">

</div>
<!--End box-->

<!-- Main row -->
<div class="row">
  <div class="col-md-8">
    <!-- USERS LIST -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Latest Events</h3>

        <div class="box-tools pull-right">
          
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
        <ul class="users-list clearfix">
          <?php
          if(!empty($new_users)){
            foreach($new_users as $new_users){
              ?>
              <li>
                <img src="<?php if(empty($new_users->avatar)) echo base_url("/uploads/files/no-thumb.png"); else echo base_url($new_users->avatar);?>" alt="User Image" width="80">
                <a class="users-list-name" href="#"><?php echo $new_users->fullName;?></a>
                <span class="users-list-date">
                  <?php
                                    $now = time();
                                    echo timespan($new_users->created, $now);
                                    ?>
                  </span>
              </li>
          <?php
            }
          }
          ?>


        </ul>
        <!-- /.users-list -->
      </div>
      <!-- /.box-body -->
      <div class="box-footer text-center">
        <a href="javascript::" class="uppercase">View All Users</a>
      </div>
      <!-- /.box-footer -->
    </div>
    <!--/.box -->
  </div><!-- /.col -->
  <div class="col-md-4">
    <!-- PRODUCT LIST -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Recently Added Products</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <ul class="products-list product-list-in-box">
          <?php
          if (!empty($new_products)){
            foreach ($new_products as $new_products){
              ?>
              <li class="item">
                <div class="product-img">
                  <img src="<?php echo $new_products->image;?>"" alt="Product Image"/>
                </div>
                <div class="product-info">
                  <a href="javascript::;" class="product-title"><?php echo $new_products->productName;?>
                    <span class="label label-info pull-right"><?php echo $new_products->price;?></span></a>
              <span class="product-description">
                <?php echo $new_products->description;?>
              </span>
                </div>
              </li><!-- /.item -->
              <?php
            }
          }
          ?>


        </ul>
      </div><!-- /.box-body -->
      <div class="box-footer text-center">
        <a href="javascript::;" class="uppercase">View All Products</a>
      </div><!-- /.box-footer -->
    </div><!-- /.box -->

    <!-- solid sales graph -->
    <div class="box box-solid bg-teal-gradient">
      <div class="box-header">
        <i class="fa fa-th"></i>

        <h3 class="box-title">Sales Graph</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
          </button>
        </div>
      </div>
      <div class="box-body border-radius-none">
        <div class="chart" id="line-chart" style="height: 250px;"></div>
      </div>
      <script>
        $(function () {

          /* Morris.js Charts */
          // Sales chart
          var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: <?php echo $sale;?>,
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Item 1'],
            lineColors: ['#efefef'],
            lineWidth: 2,
            hideHover: 'auto',
            gridTextColor: "#fff",
            gridStrokeWidth: 0.4,
            pointSize: 4,
            pointStrokeColors: ["#efefef"],
            gridLineColor: "#efefef",
            gridTextFamily: "Open Sans",
            gridTextSize: 10
          });
        });

      </script>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->
