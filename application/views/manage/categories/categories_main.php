<!--Small box (Start box) -->
<div class="row">
  <div class="col-md-12">
  <!-- Search Box -->
      <div class="col-md-6">
      <form id="user" name="user" method="POST" accept-charset="utf-8">      
        <div class="input-group">
          <input type="text" name="user_search" class="form-control" placeholder="Search Name, Email">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
        </div> 
        <input type="hidden" id="current_page" name="current_page" value=""> 
      </form>
      </div>
      <!-- End Search Box -->
  <div class="col-md-6">
    <p class="text-right">
      <button type="button" class="btn btn-lg btn-primary fa fa-user-plus" data-toggle="modal" data-target="#myModal"><span class="hidden-xs"> Create New Categories</span></button>
    </p>
  </div>
</div>
</div>
<!--End box-->

<!--Modal Create User-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="message">
        
      </div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create New Categories</h4>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Categories name" name="categoriesname" id="categoriesname">
        </div>
          
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a id="add" class="btn btn-primary">Create</a>
        </div>
      </div>
    </div>
  </div>
<!--End Modal Create New User-->

<!-- Modal Edit User-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="messageedit">
        
      </div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Categories</h4>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Categories name" name="categoriesnameedit" id="categoriesnameedit">
        </div>

        <input id="cateid" type ="hidden">
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a id="edit" class="btn btn-primary">Save</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal User-->
  
<!-- Main row -->
<div class="row">
  <div class="col-md-12">
    <!-- TABLE: LATEST ORDERS -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">List Categories</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>#ID</th>
                <th>Categories Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              <?php if(!empty($categories_list)){
                foreach ($categories_list as $category) {
                ?>
                <tr>
                  <td><?php echo $category['idCategories'];?></td>
                  <td><?php echo $category['categoryName'];?></td>
                  <td>
                    <button type="button" class="btn btn-info btn-xs editmodalbut" data-toggle="modal" data-target="#editModal" data-cateid="<?php echo $category['idCategories'];?>" data-catename="<?php echo $category['categoryName'] ?>"><i class="fa fa-edit"><span class="hidden-xs"> Edit</span></i></button>
                    <button class="popover-markup btn btn-danger btn-xs" value="<?php echo $category['idCategories']; ?>"><i class="fa fa-trash"></i><span class="hidden-xs"> Delete</span></button>
                  </td>
                  <div id="popover-head-<?php echo $category['idCategories']; ?>" class="hide">Are you sure?</div>
                  <div id="popover-content-<?php echo $category['idCategories']; ?>" class="hide">
                    <form method="POST" id="delform<?php echo $category['idCategories']; ?>" action="<?php echo base_url($this->config->item("index_page").'/manage/products/Categories/removecategory/'.$category['idCategories']); ?>">
                      <button type="submit" form="delform<?php echo $category['idCategories']; ?>"  class="btn btn-danger"><i class="fa fa-trash"></i></button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </form>
                  </div>
                </tr>
                <?php }} ?>
            </tbody>
          </table>
          <?php $this->load->view("design/pagging", $this->common->pagging); ?>
        </div><!-- /.table-responsive -->
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">

  //Create Categories Ajax
  $("#add").click(function(){
    $.ajax({
      type: "POST",
      url : "<?php echo base_url($this->config->item('index_page')."/manage/products/Categories/create_new_categories/"); ?>",
      data: {            
              categoryname: $('#categoriesname').val()
            }
    }).done(function( msg ){
        if(msg != "") {
          var result = $.parseJSON(msg);
          if(result["status"] == "OK") {
            $("#myModal").modal("hide");
            window.location="<?php echo base_url($this->config->item('index_page')."/manage/products/Categories/"); ?>";
          }
          if(result["status"] == "ERROR") {
            $("#message").empty();
            $("#message").append('<div class="alert alert-danger text-left">'+result["message"]+'</div>');
          }
        }
      });
  });

  //Edit Categories Ajax
  $("#edit").click(function(){
    $.ajax({
      type: "POST",
      url : "<?php echo base_url($this->config->item('index_page')."/manage/products/Categories/editcategory/"); ?>",
      data: {
              categoryid: $('#cateid').val(),
              categoryname: $('#categoriesnameedit').val()
            }
      }).done(function(msg){
        if(msg != "") {
          var result = $.parseJSON(msg);
          if(result["status"] == "OK") {
            $("#myModal").modal("hide");
            window.location="<?php echo base_url($this->config->item('index_page')."/manage/products/Categories/"); ?>";
          }
          if(result["status"] == "ERROR") {
            $("#messageedit").empty();
            $("#messageedit").append('<div class="alert alert-danger text-left">'+result["message"]+'</div>');
          }
        }
      });
  });

  $(function(){

        $('.popover-markup').popover({
            trigger: 'focus',
            placement: 'top',
            html : true,
            title: function() {
                var id = $(this).val();
                return $("#popover-head-"+id).html();
            },
            content: function() {
                var id = $(this).val();
                return $("#popover-content-"+id).html();
            }
        });
    });
  
  //Passing data to Modal
  $(document).ready(function(){
    $(document).on("click", ".editmodalbut", function () {
     var catename = $(this).data('catename');
     var cateid = $(this).data('cateid');
     $(".modal-body #categoriesnameedit").val(catename);
     $(".modal-body #cateid").val(cateid);
   });

    $(".pagination>li>a").click(function() {
      if($(this).attr("page") != "")
        pagging("user", $(this).attr("page"));
    });
  
});
</script>