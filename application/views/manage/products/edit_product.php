<div class="box box-warning">
  <form method="post">
                  <div class="box-body">
                      <div id="message">
                          <?php
                          if (!empty($message)){
                              echo '<div class="alert alert-danger text-left">'.$message."</div>";
                          }
                          ?>
                      </div>
                      <div class="row">
                        <?php if(!empty($old)){
                          foreach ($old as $product) {
                           ?> 
                          
                          <div class="col-md-3">
                              <script type="text/javascript">
                                  function BrowseServer()
                                  {
                                      var finder = new CKFinder();
                                      finder.basePath = '../';
                                      finder.selectActionFunction = SetFileField;
                                      finder.popup();
                                  }
                                  function SetFileField( fileUrl )
                                  {
                                      document.getElementById('xFilePath').value = fileUrl;
                                      document.getElementById('imgPreview').src = fileUrl;
                                  }
                              </script>
                              <div class="form-group">
                                  <label>Image</label>
                                  <div class="input-group">

                                      <input type="text" class="form-control" id="xFilePath" name="image" value="<?php echo $product['image']; ?>" placeholder="Enter ...">
                                          <span class="input-group-btn">
                                            <button class="btn btn-default btn-flat" type="button" onclick="BrowseServer();"><i class="fa fa-camera"></i></button>
                                          </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                <?php if(!empty($product['image'])){ ?>
                                <img src="<?php echo $product['image']; ?>" width="100%" id="imgPreview"/>
                                <?php }else{ ?>
                                  <img src="/uploads/images/no-thumb.png" width="100%" id="imgPreview"/>
                                <?php } ?>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="form-group">
                                  <label>Product Name</label>
                                  <input type="text" class="form-control txt_title" name="productname" value="<?php echo $product['productName']; ?>" placeholder="Enter ...">
                              </div>

                              <div class="form-group">
                                <label>Category</label>
                                <select class="form-control" style="width: 100%;" tabindex="-1" aria-hidden="true" name="categoriesselect" id="categoriesselect">
                                  <option value=""></option>
                                  <?php if(!empty($categories_list)){?>
                                  <?php foreach ($categories_list as $category_list) { ?>
                                  <option <?php if($category_list['idCategories']==$product['categoryId']) echo "selected"?> value="<?php echo $category_list['idCategories']?>"><?php echo $category_list['categoryName'];?></option>
                                  <?php }?> 
                                  <?php } ?>
                                </select>
                              </div>

                              <div class="form-group">
                                  <label>Alias</label>
                                  <input type="text" class="form-control  txt_alias" value="<?php echo $product['alias']; ?>" name="alias" placeholder="Enter ...">
                              </div>
                              <script>
                                  $(document).ready(function(){
                                      $(".txt_title").change(function(){
                                          $('.txt_alias').val(locdau($(this).val()));
                                      });
                                  });

                                  function locdau(str)
                                  {
                                      str= str.toLowerCase();
                                      str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
                                      str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
                                      str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
                                      str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
                                      str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
                                      str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
                                      str= str.replace(/đ/g,"d");
                                      str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
                                      str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
                                      str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
                                      return str;
                                  }

                              </script>
                              <div class="form-group">
                                  <label>Price</label>
                                  <input type="text" class="form-control" value="<?php echo $product['price']; ?>"name="productprice" id="productprice" placeholder="Enter ...">
                              </div>
                              <div class="form-group">
                                  <label>Discount</label>
                                  <input type="text" class="form-control" value="<?php echo $product['discount']; ?>" name="productdiscount" id="productdiscount" placeholder="Enter ...">
                              </div>
                              <div class="form-group">
                                  <label>Description</label>
                                  <input type="text" class="form-control" value="<?php echo $product['description']; ?>" name="description" id="description" placeholder="Enter ...">
                              </div>
                              <div class="form-group">
                                  <label>Details</label>
                                  <script src="<?=base_url('assets/plugins/ckeditor/ckeditor.js');?>"></script>
                                  <textarea name="detail" id="editor1">
                                    <?php echo $product['detail']; ?>
                                  </textarea>
                                  <script>
                                      // Replace the <textarea id="editor1"> with a CKEditor
                                      // instance, using default configuration.
                                      CKEDITOR.replace( 'editor1' );
                                  </script>
                              </div>

                          </div>
                        <?php }}?>
                      </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <a href="<?php echo base_url($this->config->item('index_page')."/manage/products/Products") ?>" class="btn btn-default">Cancel</a>
                    <!-- <button type="submit" id="addproduct" class="btn btn-info pull-right">Save</button> -->
                    <input type="submit" name="editproduct" value="Save" class="btn btn-info pull-right">
                  </div>
                </form>
</div>
<script type="text/javascript">
$('#categoriesselect').select2({
  placeholder: "Select Category",
  allowClear: true
});

$(function($){
  var cfgCulture = 'vi-VN';
  $.preferCulture(cfgCulture);
  $('#productprice').maskMoney();
});
</script>