<!-- Main row -->
<div class="row">
  <div class="col-md-12">
    <!-- TABLE: LATEST ORDERS -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">List Products</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Image</th>
                <th>Products Name</th>
                <th>Price</th>
                <th>Updated At</th>
                <th>Decriptions</th>
                <th>Details</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              <?php if(!empty($products_list)){
                foreach ($products_list as $product) {
                ?>
                <tr>
                  <td><img src="<?php echo $product['image']; ?>" height="100" width="100"> </td>
                  <td><?php echo $product['productName'];?></td>
                  <td><?php echo $product['price'];?></td>
                  <td><?php echo $product['description']; ?> </td>
                  <td><?php echo $product['detail']; ?> </td>
                  <td><?php echo timespan($product['created'],time());?></td>
                  <td>
                    <a type="button"  href="<?php echo base_url($this->config->item('index_page').'/manage/products/Products/editProduct/'.$product['idProducts']); ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"><span class="hidden-xs"> Edit</span></i></a>
                    <button class="popover-markup btn btn-danger btn-xs" value="<?php echo $product['idProducts']; ?>"><i class="fa fa-trash"></i><span class="hidden-xs"> Delete</span></button>
                  </td>
                  <div id="popover-head-<?php echo $product['idProducts']; ?>" class="hide">Are you sure?</div>
                  <div id="popover-content-<?php echo $product['idProducts']; ?>" class="hide">
                    <form method="POST" id="delform<?php echo $product['idProducts']; ?>" action="<?php echo base_url($this->config->item("index_page").'/manage/users/Main_User/removeUser/'.$product['idProducts']); ?>">
                      <button type="submit" form="delform<?php echo $product['idProducts']; ?>"  class="btn btn-danger"><i class="fa fa-trash"></i></button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </form>
                  </div>
                </tr>
                <?php }} ?>
            </tbody>
          </table>
          <?php $this->load->view("design/pagging", $this->common->pagging); ?>
        </div><!-- /.table-responsive -->
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">
$(function(){
        $('.popover-markup').popover({
            trigger: 'focus',
            placement: 'top',
            html : true,
            title: function() {
                var id = $(this).val();
                return $("#popover-head-"+id).html();
            },
            content: function() {
                var id = $(this).val();
                return $("#popover-content-"+id).html();
            }
        });
    });
</script>