<!--Small box (Start box) -->
<div class="row">
  <div class="col-md-12">
  <!-- Search Box -->
      <div class="col-md-6">
      <form id="user" name="user" method="POST" accept-charset="utf-8">      
        <div class="input-group">
          <input type="text" name="user_search" class="form-control" placeholder="Search Name, Email">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
        </div> 
        <input type="hidden" id="current_page" name="current_page" value=""> 
      </form>
      </div>
      <!-- End Search Box -->
  <div class="col-md-6">
    <p class="text-right">
      <a href="<?php echo base_url($this->config->item("index_page").'/manage/products/Products/create_new_product') ?>" class="btn btn-lg btn-primary fa fa-tag"><span class="hidden-xs"> Create New Product</span></a>
    </p>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <select class="form-control" style="width: 100%;" tabindex="-1" aria-hidden="true" name="categoriesselect" id="categoriesselect">
      <option value="0"> All Products</option>
      <?php if(!empty($categories_list)){?>
      <?php foreach ($categories_list as $category_list) { ?>
      <option <?php if($this->input->post('categoriesselect') == $category_list['idCategories']) echo "selected"?> value="<?php echo $category_list['idCategories']?>"><?php echo $category_list['categoryName'];?></option>
      <?php }?> 
      <?php } ?>
    </select>
  </div>
</div>
</div>
<!--End box-->
  
<!-- Main row -->
<div class="row" id="infoallproducts">
  <div class="col-md-12">
    <!-- TABLE: LATEST ORDERS -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">List Products</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Image</th>
                <th>Products Name</th>
                <th>Price</th>
                <th>Updated At</th>
                <th>Decriptions</th>
                <th>Details</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              <?php if(!empty($products_list)){
                // print_r($products_list);exit();
                foreach ($products_list as $product) {
                ?>
                <tr>
                  <td><img src="<?php echo $product['image']; ?>" height="100" width="100"> </td>
                  <td><?php echo $product['productName'];?></td>
                  <td><?php echo $product['price'];?></td>
                  <td><?php echo $product['description']; ?> </td>
                  <td><?php echo $product['detail']; ?> </td>
                  <td><?php echo timespan($product['created'],time());?></td>
                  <td>
                    <a type="button" href="<?php echo base_url($this->config->item('index_page').'/manage/products/Products/editProduct/'.$product['idProducts']); ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"><span class="hidden-xs"> Edit</span></i></a>
                    <button class="popover-markup btn btn-danger btn-xs" value="<?php echo $product['idProducts']; ?>"><i class="fa fa-trash"></i><span class="hidden-xs"> Delete</span></button>
                  </td>
                  <div id="popover-head-<?php echo $product['idProducts']; ?>" class="hide">Are you sure?</div>
                  <div id="popover-content-<?php echo $product['idProducts']; ?>" class="hide">
                    <form method="POST" id="delform<?php echo $product['idProducts']; ?>" action="<?php echo base_url($this->config->item("index_page").'/manage/products/Products/removeProduct/'.$product['idProducts']); ?>">
                      <button type="submit" form="delform<?php echo $product['idProducts']; ?>"  class="btn btn-danger"><i class="fa fa-trash"></i></button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </form>
                  </div>
                </tr>
                <?php }} ?>
            </tbody>
          </table>
          <?php $this->load->view("design/pagging", $this->common->pagging); ?>
        </div><!-- /.table-responsive -->
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->
<div id="infoproducts"></div>
<script type="text/javascript">
$('#categoriesselect').select2({
  placeholder: "Select Category",
  allowClear: true
});

  $(function(){
        $('.popover-markup').popover({
            trigger: 'focus',
            placement: 'top',
            html : true,
            title: function() {
                var id = $(this).val();
                return $("#popover-head-"+id).html();
            },
            content: function() {
                var id = $(this).val();
                return $("#popover-content-"+id).html();
            }
        });
    });
  
  $(document).ready(function(){
 $("#categoriesselect").change(function () {
  loadproduct();
  });

 function loadproduct(){
  $.ajax({
    url: "<?php echo base_url($this->config->item('index_page')."/manage/products/Products/load_product_by_categories/"); ?>" + "/" + $("#categoriesselect").val() ,
    cache: false,
    dataType: 'html',
    error: function() {
      alert('PRODUCTS NOT FOUND !');
    },
    success: function(result) {
      $('#infoproducts').html(result);
      $('#infoallproducts').hide();
    }
  }); 
}

    $(".pagination>li>a").click(function() {
      if($(this).attr("page") != "")
        pagging("user", $(this).attr("page"));
    });
  
});
</script>