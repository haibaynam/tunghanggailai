<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Events</h3>

                <div class="box-tools">
                    <a href="<?php echo base_url($this->config->item('index_page')."/Banner/new_banner/"); ?>" class="btn btn-primary"><i
                            class="fa fa-plus-square"></i> Add Banner</a>
                </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body table-responsive no-padding">
                <form id="event" method="POST" accept-charset="utf-8">
                    <div class="box-tools" style="padding: 8px">
                        <div class="input-group" style="width: 150px;">
                            <input type="text" name="search_key" id="search-text" class="form-control input-sm pull-left"
                                   placeholder="Search">

                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default" id="search-event"><i class="fa fa-search"></i>
                                </button>
                            </div>
                            <input type="hidden" id="current_page" name="current_page" value="">
                        </div>
                    </div>
                </form>
                <table class="table table-hover">
                    <tbody id="table-event">

                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Position</th>
                        <th>Start Day</th>
                        <th>Expired Day</th>
                        <th>Link</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    if (!empty($banner)) {
                        foreach ($banner as $banner) {
                            ?>
                            <tr>
                                <td><?php echo $banner['idBanner']; ?></td>
                                <td><?php echo $banner['title']; ?></td>
                                <td><?php echo $banner['position'];?></td>
                                <td><?php echo $banner['startDate'];?></td>
                                <td><?php echo $banner['endDate'];?></td>
                                <td><?php echo $banner['link']; ?></td>

                                <td>
                                    <a href="" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i> View</a>
                                    <a href="<?= base_url('index.php/Banner'); ?>/update_banner/<?php echo $banner['idBanner']; ?>"
                                       class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    <button class="popover-markup btn btn-danger btn-xs"
                                            value="<?php echo $banner['idBanner']; ?>"><i
                                            class="fa fa-trash"></i><span class="hidden-xs"> Delete</span></button>
                                </td>

                                <!--                              <div id="popover-head--->
                                <?php //echo $event->idInformation;?><!--" class="hide">Confirm</div>-->
                                <div id="popover-content-<?php echo $banner['idBanner']; ?>" class="hide">
                                    <form method="POST" id="delform<?php echo $banner['idBanner']; ?>"
                                          action="<?php echo base_url($this->config->item("index_page") . '/Banner/delete_banner/' . $banner['idBanner']); ?>">
                                        <button type="submit" form="delform<?php echo $banner['idBanner']; ?>"
                                                class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i
                                                class="fa fa-times"></i></button>
                                    </form>

                                    <script>
                                        $(function () {

                                            $('.popover-markup').popover({
                                                trigger: 'focus',
                                                placement: 'top',
                                                html: true,
                                                title: function () {
                                                    var id = $(this).val();
                                                    return $("#popover-head-" + id).html();
                                                },
                                                content: function () {
                                                    var ids = $(this).val();
                                                    return $("#popover-content-" + ids).html();
                                                }
                                            });
                                        });
                                    </script>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>

                <?php $this->load->view("design/pagging", $this->common->pagging); ?>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
<script type="application/javascript">
    $(".pagination>li>a").click(function() {
        if($(this).attr("page") != "")
            pagging("event", $(this).attr("page"));
    });
</script>
