<div class="box box-warning">
  <div class="box-header with-border">
      <h3 class="box-title">Add New Banner</h3>
  </div><!-- /.box-header -->
  <!-- form start -->

  <form role="form" action="#" method="post">
                  <div class="box-body">
                      <div id="message">
                          <?php
                          if (!empty($status)){
                              echo '<div class="alert alert-danger text-left">'.$message."</div>";
                          }
                          ?>
                      </div>
                      <div class="row">
                          <div class="col-md-3">
                              <script type="text/javascript">
                                  function BrowseServer()
                                  {
                                      var finder = new CKFinder();
                                      finder.basePath = '../';
                                      finder.selectActionFunction = SetFileField;
                                      finder.popup();
                                  }
                                  function SetFileField( fileUrl )
                                  {
                                      document.getElementById( 'xFilePath' ).value = fileUrl;
                                      document.getElementById('imgPreview').src = fileUrl;
                                  }
                              </script>
                              <div class="form-group">
                                  <label>Image</label>
                                  <div class="input-group">

                                      <input type="text" class="form-control" id="xFilePath" name="image" placeholder="Enter ...">
                                          <span class="input-group-btn">
                                            <button class="btn btn-default btn-flat" type="button" onclick="BrowseServer();"><i class="fa fa-camera"></i></button>
                                          </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <img src="<?php echo base_url("uploads/files/no-thumb.png");?>" width="100%" id="imgPreview"/>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="form-group">
                                  <label>Title</label>
                                  <input type="text" class="form-control txt_title" name="title" value="" placeholder="Enter ...">
                              </div>
                              <div class="form-group">
                                  <label>Expired Day</label>
                                  <div class="input-daterange input-group" id="datepicker">

                                      <input type="text" class="form-control" name="startDate" />
                                      <span class="input-group-addon">to</span>
                                      <input type="text" class="form-control" name="endDate" />
                                  </div>
                              </div>

                              <script>

                                  $(document).ready(function(){
                                      $('.input-daterange').datepicker({
                                      });
                                      $(".txt_title").change(function(){
                                          $('.txt_alias').val(locdau($(this).val()));
                                      });
                                  });

                              </script>
                              <div class="form-group">
                                  <label>Link</label>
                                  <input type="text" class="form-control" name="link" value="" placeholder="Enter ...">
                              </div>
                              <div class="form-group">
                                  <label>Position</label>
                                  <select class="form-control select2" name="position" id="position">
                                      <option value="1">Header Left</option>
                                      <option value="2" selected>Header Right</option>
                                      <option value="3">Body Left Top News</option>
                                      <option value="4">Body Left Center News</option>
                                      <option value="5">Body Left Bottom News</option>
                                      <option value="6">Body Left Top Detail</option>
                                      <option value="7">Body Left Center Detail</option>
                                      <option value="8">Body Left Bottom Detail</option>
                                      <option value="9">Body Right Top News</option>
                                      <option value="10">Body Right Center News</option>
                                      <option value="11">Body Right Bottom News</option>
                                      <option value="12">Body Right Top Detail</option>
                                      <option value="13">Body Right Center Detail</option>
                                      <option value="14">Body Right Bottom Detail</option>
                                      <option value="15">Body Center</option>
                                      <option value="16">Body Top</option>
                                      <option value="17">Body Bottom</option>
                                  </select>
                              </div>
                          </div>

                      </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <a href="<?php echo base_url($this->config->item('index_page')."/Banner/") ?>" class="btn btn-default">Cancel</a>
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                  </div>
                </form>
</div>
<script>
    $('#position').select2({
        placeholder: "Role",
        minimumResultsForSearch: Infinity,
        allowClear: true
    });
</script>