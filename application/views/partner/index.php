<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Partners</h3>

                <div class="box-tools">
                    <a href="<?php echo base_url($this->config->item('index_page')."/Partner/new_partner/"); ?>" class="btn btn-primary"><i
                            class="fa fa-plus-square"></i> Add Partner</a>
                </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body table-responsive no-padding">
                <form id="event" method="POST" accept-charset="utf-8">
                    <div class="box-tools" style="padding: 8px">
                        <div class="input-group" style="width: 150px;">
                            <input type="text" name="search_key" id="search-text" class="form-control input-sm pull-left"
                                   placeholder="Search">

                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default" id="search-event"><i class="fa fa-search"></i>
                                </button>
                            </div>
                            <input type="hidden" id="current_page" name="current_page" value="">
                        </div>
                    </div>
                </form>
                <table class="table table-hover">
                    <tbody id="table-event">

                    <tr>
                        <th>ID</th>
                        <th>Logo</th>
                        <th>Name</th>
                        <th>Created</th>
                        <th>Update at</th>
                        <th>Link</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    if (!empty($partner)) {
                        foreach ($partner as $partner) {
                            ?>
                            <tr>
                                <td><?php echo $partner['idPartner']; ?></td>
                                <td><img src="<?php echo $partner['image']; ?>" width="30" height="30"/></td>
                                <td><?php echo $partner['name'];?></td>
                                <td>
                                    <?php
                                    $now = time();
                                    echo timespan($partner['created'], $now);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($partner['updateAt'])){
                                        $now = time();
                                        echo timespan($partner['updateAt'], $now);
                                    }

                                    ?>
                                </td>
                                <td><?php echo $partner['link'];?></td>

                                <td>
                                    <a href="" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i> View</a>
                                    <a href="<?= base_url('index.php/Partner'); ?>/update_partner/<?php echo $partner['idPartner']; ?>"
                                       class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    <button class="popover-markup btn btn-danger btn-xs"
                                            value="<?php echo $partner['idPartner']; ?>"><i
                                            class="fa fa-trash"></i><span class="hidden-xs"> Delete</span></button>
                                </td>

                                <!--                              <div id="popover-head--->
                                <?php //echo $event->idInformation;?><!--" class="hide">Confirm</div>-->
                                <div id="popover-content-<?php echo $partner['idPartner']; ?>" class="hide">
                                    <form method="POST" id="delform<?php echo $partner['idPartner']; ?>"
                                          action="<?php echo base_url($this->config->item("index_page") . '/Partner/delete_partner/' . $partner['idPartner']); ?>">
                                        <button type="submit" form="delform<?php echo $partner['idPartner']; ?>"
                                                class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i
                                                class="fa fa-times"></i></button>
                                    </form>

                                    <script>
                                        $(function () {

                                            $('.popover-markup').popover({
                                                trigger: 'focus',
                                                placement: 'top',
                                                html: true,
                                                title: function () {
                                                    var id = $(this).val();
                                                    return $("#popover-head-" + id).html();
                                                },
                                                content: function () {
                                                    var ids = $(this).val();
                                                    return $("#popover-content-" + ids).html();
                                                }
                                            });
                                        });
                                    </script>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>

                <?php $this->load->view("design/pagging", $this->common->pagging); ?>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
<script type="application/javascript">
    $(".pagination>li>a").click(function() {
        if($(this).attr("page") != "")
            pagging("event", $(this).attr("page"));
    });
</script>
