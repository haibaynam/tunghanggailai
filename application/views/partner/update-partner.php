<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Update Partner</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <?php
        if (!empty($detail)) {
        foreach ($detail as $detail) {
    ?>
    <form role="form" action="#" method="post">
        <div class="box-body">
            <div id="message">
                <?php
                if (!empty($status)){
                    echo '<div class="alert alert-danger text-left">'.$message."</div>";
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <script type="text/javascript">
                        function BrowseServer()
                        {
                            var finder = new CKFinder();
                            finder.basePath = '../';
                            finder.selectActionFunction = SetFileField;
                            finder.popup();
                        }
                        function SetFileField( fileUrl )
                        {
                            document.getElementById( 'xFilePath' ).value = fileUrl;
                            document.getElementById('imgPreview').src = fileUrl;
                        }
                    </script>
                    <div class="form-group">
                        <label>Image</label>
                        <div class="input-group">

                            <input type="text" class="form-control" id="xFilePath" name="image" placeholder="Enter ..." value="<?php echo $detail->image;?>">
                                          <span class="input-group-btn">
                                            <button class="btn btn-default btn-flat" type="button" onclick="BrowseServer();"><i class="fa fa-camera"></i></button>
                                          </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <img src="<?php echo $detail->image;?>" width="100%" id="imgPreview"/>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control txt_title" name="name" value="<?php echo $detail->name;?>" placeholder="Enter ...">
                    </div>

                    <div class="form-group">
                        <label>Link</label>
                        <input type="text" class="form-control" name="link" value="<?php echo $detail->link;?>" placeholder="Enter ...">
                    </div>
                </div>

            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="<?php echo base_url($this->config->item('index_page')."/Partner/") ?>" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-info pull-right">Save</button>
        </div>
    </form>
</div>

<script>
    $('#position').select2({
        placeholder: "Role",
        minimumResultsForSearch: Infinity,
        allowClear: true
    });
    $('#position').select2("val","<?php echo $detail->position;?>");

</script>
<?php
}
}
?>