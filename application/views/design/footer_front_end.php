<footer id="footer-section" class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="widget">
                    <div class="widget-inner">
                        <h3 class="widget-title">Theo dõi chúng tôi</h3>
                        <div class="social-icons">
                        <p>
                            <a href="skype:haibaynam?chat"><img src="<?=base_url("/uploads/images/skype.png");?>" width="40"/></a>
                        </p>
                        <p>
                            <a href="https://www.facebook.com/tunghangcoffee81/"><img src="<?=base_url("/uploads/images/facebook.png");?>" width="40"/></a>
                        </p>
                        <p>
                            <a><img src="<?=base_url("/uploads/images/google-plus.png");?>" width="40"/></a>
                        </p>
                        <p>
                            <a><img src="<?=base_url("/uploads/images/twitter.png");?>" width="40"/></a>
                        </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="widget">
                    <div class="widget-inner">
                        <h3 class="widget-title">Phân Phối </h3>
                        <table>
                            <tr>
                                <td><strong>Đ/C</strong></td>
                                <td>: An Hải 16 - Quận Sơn Trà - TP Đà Nẵng</td>
                            </tr>
                            <tr>
                                <td><strong>SĐT</strong></td>
                                <td>: 01232.80.80.90</td>
                            </tr>                            
                        </table>
                        <hr/>
                        <table>
                            <tr>
                                <td><strong>Đ/C</strong></td>
                                <td>  : 105 Trần Văn Đang - Phường 9 - Quận 3 - TPHCM</td>
                            </tr>
                            <tr>
                                <td><strong>SĐT</strong></td>
                                <td> : 0934.28.29.33 - 0963.43.89.33</td>
                            </tr>                            
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget">
                    <div class="widget-inner">
                        <h3 class="widget-title">Thông tin doanh nghiệp</h3>
                        <div class="var-section">
                            <p>CÔNG TY TNHH MTV TÙNG HẰNG GIA LAI<br/>
                            <table>
                                <tr>
                                    <td><strong>Mã số doanh nghiệp</strong></td>
                                    <td> : 5900939320</td>
                                </tr>
                                <tr>
                                    <td><strong>Ngày cấp mã DN</strong></td>
                                    <td> : 25/09/2012</td>
                                </tr>
                                <tr>
                                    <td><strong>Chủ doanh nghiệp</strong></td>
                                    <td> : <a href="#">Lê Thanh Tùng</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<script src=<?=base_url('assets/js/boostrap.min.js');?> type='text/javascript'></script>
<script src=<?=base_url('assets/js/plugin.js');?> type='text/javascript'></script>
<script src=<?=base_url('assets/js/main.js');?> type='text/javascript'></script>
</body>
</html>