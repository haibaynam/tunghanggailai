<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        CÔNG TY TNHH MTV TÙNG HẰNG GIA LAI
    </title>


    <meta name="description" content="CÔNG TY TNHH MTV TÙNG HẰNG GIA LAI">


    <!-- Bootstrap -->
    <link href='<?=base_url('assets/css/style.css');?>' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?=base_url('assets/dist/js/common.js');?>" type="text/javascript"></script>
    <script src='<?=base_url('assets/js/jquery-1.11.min.js');?>' type='text/javascript'></script>
</head>
<body>
<div id="page" class="hfeed site">
    <header id="masthead" class="site-header navbar-fixed-top">
        <div class="header-navigation">
            <div class="container-fluid">
                <div class="row">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".site-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo navbar-brand">
                        <a href="<?php echo base_url(); ?>">
                            <img src="<?=base_url("/uploads/images/logo.png");?>"/>
                        </a>
                    </div>
                    <nav id="primary-navigation" class="site-navigation navbar-collapse collapse" role="navigation">
                        <div class="nav-menu">
                            <ul class="menu">


                                <li class="" id="homepage"><a href="<?=base_url("index.php/Home");?>">Trang chủ</a></li>



                                <li class="" id="introduction"><a href="<?=base_url("index.php/Introduction");?>">Giới thiệu</a></li>



                                <li class="" id="news"><a href="<?=base_url("index.php/News");?>">Tin tức</a></li>



                                <li class="" id="contact"><a href="<?=base_url("index.php/Contact");?>">Phân phối</a></li>

                            </ul>

                        </div>
                    </nav>
                        <div class="contact-mobile"><img src="<?=base_url("/uploads/images/lienhe.png");?>"/></div>
                    
                </div>
            </div>
        </div>
    </header>
</div>
<!--    End header-->