<?php 
  $pages = intval($total_rows/$rows_per_page);
  $pages += $total_rows%$rows_per_page>0?1:0;
  $tem = $current_page * $rows_per_page;
  $from = $tem + 1;
  $to = $tem + $rows_per_page;
  $to = $to < $total_rows ? $to : $total_rows;
?>

<?php if($total_rows): ?>

  <div class="col-md-12">
  <div class="dataTables_info">
    <?php 
      echo "Showing ".$from." to ".$to." of ".$total_rows." entries"; 
    ?>
  </div>
  </div>
  <?php if($total_rows>$rows_per_page): ?>
    <div class="box-footer clearfix">
      <ul class="pagination pagination-sm no-margin pull-right">
        <?php if($current_page==0): ?>
        <li class="prev disabled"><a page="" href="javascript:void(0)">&#8592; Previous</a></li>
        <?php endif ?>
        <?php if($current_page!=0): ?>
        <li class="prev"><a page="<?php echo $current_page-1>0?$current_page-1:0; ?>" href="javascript:void(0)">← Previous</a></li>
        <?php endif ?>
        <?php
          $start = $current_page - 5;
          $end = $start < 0 ? 9 : $current_page + 5;
          $end = $end <= $pages ? $end : $pages;
          if($start>1) {
            echo "<li>...</li>";
          }
          $i = $start < 1 ? 1:$start;
         for ($i; $i <= $end; $i++) {
          if($i == $current_page+1) {
            echo "<li class='active'><a page='' href='javascript:void(0)'>".$i."</a></li>";
          } else {
            echo "<li><a page='".($i-1)."' href='javascript:void(0)'>".$i."</a></li>";
          }
          }// End loop
          if(($current_page + 5) < $pages) {
            echo "<li>...</li>";
          }
        ?>
        <?php if($current_page==$pages-1): ?>
          <li class="next disabled"><a page="" href="javascript:void(0);">Next &#8594;</a></li>
        <?php endif ?>
        <?php if($current_page!=$pages-1): ?>
          <li class="next"><a page="<?php if($current_page!=$pages-1) echo $current_page+1; ?>" href="javascript:void(0)">Next → </a></li>
        <?php endif ?>
      </ul>
    </div>
  <?php endif ?>

<?php endif ?>
<script type="text/javascript">
  $(document).ready(function() {
    $(".pagination>li").unbind();
  });
</script>