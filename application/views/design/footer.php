        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <strong>Copyright &copy; 2014-2015 <a href="<?php echo base_url(); ?>">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

      <?php 
  // Display notification message
  if($this->session->flashdata('message')): ?>
  <script type="text/javascript">
    $(document).ready(function(){
      noty(<?php echo $this->session->flashdata('message'); ?>);
    });
  </script>
<?php endif ?>
  <script src="<?=base_url('assets/dist/js/common.js');?>"></script>
  </body>
</html>
