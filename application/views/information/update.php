<div class="box box-warning">
  <div class="box-header with-border">
      <h3 class="box-title">Update information</h3>
  </div><!-- /.box-header -->
  <!-- form start -->
  <?php 
    if (!empty($detail)) {
      foreach ($detail as $detail) {
      ?>
        <form role="form" action="#" method="post">
                  <div class="box-body">
                      <div id="message">
                          <?php
                          if (!empty($status)){
                              echo '<div class="alert alert-danger text-left">'.$message."</div>";
                          }
                          ?>
                      </div>
                      <div class="form-group">
                          <label>Title</label>
                          <input type="text" class="form-control txt_title" name="title" value="<?php echo $detail->title;?>" placeholder="Enter ...">
                      </div>
                      <div class="form-group">
                          <label>Alias</label>
                          <input type="text" class="form-control txt_alias" name="alias" value="<?php echo $detail->alias;?>" placeholder="Enter ...">
                      </div>
                    <div class="form-group">
                      <label>Ref Link</label>
                      <input type="text" class="form-control" name="refLink" value="<?php echo $detail->refLink;?>" placeholder="Enter ...">
                    </div>
                    <div class="form-group">
                      <label>Types</label>
                      <select class="form-control" name="type">
                        <option value="0" selected>Infor</option>
                        <option value="1">Event</option>
                        <option value="2">Popup</option>
                      </select>
                    </div>
                    
                        <label>Content</label>
                        <script src="<?=base_url('assets/plugins/ckeditor/ckeditor.js');?>"></script>
                        <textarea name="description" id="editor1" rows="10" cols="80">
                            <?php echo $detail->description;?>
                        </textarea>
                      <script>
                          // Replace the <textarea id="editor1"> with a CKEditor
                          // instance, using default configuration.
                          CKEDITOR.replace('editor1', {
                              filebrowserBrowseUrl: '/browser/browse.php',
                              filebrowserImageBrowseUrl: '/browser/browse.php?type=Images',
                              filebrowserUploadUrl: '/uploader/upload.php',
                              filebrowserImageUploadUrl: '/uploader/upload.php?type=Images',
                              filebrowserWindowWidth: '900',
                              filebrowserWindowHeight: '400',
                              filebrowserBrowseUrl: '<?=base_url('assets/plugins//ckfinder/ckfinder.html?Type=Images');?>',
                              filebrowserImageBrowseUrl: '<?=base_url('assets/plugins/ckfinder/ckfinder.html?Type=Images');?>',
                              filebrowserFlashBrowseUrl: '<?=base_url('assets/plugins/ckfinder/ckfinder.html?Type=Flash');?>',
                              filebrowserUploadUrl: '<?=base_url('assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files');?>',
                              filebrowserImageUploadUrl: '<?=base_url('assets/plugins/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images');?>',
                              filebrowserFlashUploadUrl: '<?=base_url('assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash');?>'
                          });﻿
                      </script>

                      <script>
                          $(document).ready(function(){
                              $(".txt_title").change(function(){
                                  $('.txt_alias').val(locdau($(this).val()));
                              });
                          });

                          function locdau(str)
                          {
                              str= str.toLowerCase();
                              str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
                              str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
                              str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
                              str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
                              str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
                              str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
                              str= str.replace(/đ/g,"d");
                              str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
                              str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
                              str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
                              return str;
                          }

                      </script>
                                      
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <a href="../" class="btn btn-default">Cancel</a>
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                  </div>
                </form>
      <?php
      }
    }
  ?>
</div>