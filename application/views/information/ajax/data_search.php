<tr>
    <th>ID</th>
    <th>Title</th>
    <th>Updated</th>
    <th>Link</th>
    <th>Action</th>
</tr>
<?php
                      if (!empty($data_search)) {
                          foreach ($data_search as $data_search) {
                              ?>
                              <tr>
                                  <td><?php echo $data_search->idInformation;?></td>
                                  <td><?php echo $data_search->title;?></td>
                                  <td>
                                      <?php
                                      $now = time();
                                      echo timespan($data_search->created, $now);
                                      ?>
                                  </td>
                                  <td><?php echo $data_search->refLink;?></td>

                                  <td>
                                      <a href="" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i> View</a>
                                      <a href="<?=base_url('index.php/Infor');?>/update_event/<?php echo $data_search->idInformation;?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                      <button class="popover-markup btn btn-danger btn-xs" value="<?php echo $data_search->idInformation;?>"><i class="fa fa-trash"></i><span class="hidden-xs"> Delete</span></button>
                                  </td>

                                  <!--                              <div id="popover-head---><?php //echo $event->idInformation;?><!--" class="hide">Confirm</div>-->
                                  <div id="popover-content-<?php echo $data_search->idInformation;?>" class="hide">
                                      <form method="POST" id="delform<?php echo $data_search->idInformation;?>" action="<?php echo base_url($this->config->item("index_page").'/Infor/delete_event/'.$data_search->idInformation); ?>">
                                          <button type="submit" form="delform<?php echo $data_search->idInformation;?>"  class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                          <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i class="fa fa-times"></i></button>
                                      </form>

                                      <script>
                                          $(function(){

                                              $('.popover-markup').popover({
                                                  trigger: 'focus',
                                                  placement: 'top',
                                                  html : true,
                                                  title: function() {
                                                      var id = $(this).val();
                                                      return $("#popover-head-"+id).html();
                                                  },
                                                  content: function() {
                                                      var ids = $(this).val();
                                                      return $("#popover-content-"+ids).html();
                                                  }
                                              });
                                          });
                                      </script>
                              </tr>
                              <?php
                          }
                      }
                    ?>