<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="head-title">
    <div class="container">
        <div class="row">
            <h2 class="page-title">Phân phối</h2>
        </div>
    </div>
</div>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="content-area col-md-8" id="primary">
                <div class="site-content" id="content">
                    <article class="post hentry">
                        <header class="entry-header">
                            <div class="entry-map">
                                <div class="map">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.369650240466!2d106.67655074985615!3d10.782974561984885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f29a5e38709%3A0x2e0deb370319a3fc!2zMTA1IFRy4bqnbiBWxINuIMSQYW5nLCBwaMaw4budbmcgOSwgUXXhuq1uIDMsIEjhu5MgQ2jDrSBNaW5oLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1452800504240" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <h1 class="entry-title">
                                <a></a>
                            </h1>
                        </header>
                        <div class="entry-content">
                            <p>
                            </p>
                            <p>&nbsp;</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>
                                        <strong>Trụ sở chính:</strong><br>
                                        Iagrai - Gia Lai<br>
                                        Thôn I - Xã IaHrung - Huyện Iagrai - Gia Lai<br>
                                        Điện thoại: 0986 80 80 90<br>
                                        Email: tunghanggialai@gmail.com<br>

                                    </p>
                                    <p>
                                        <strong>Thành Phồ Hồ Chí Minh</strong><br>
                                        105 Trần Văn Đang - Phường 9 - Quận 3 - TPHCM<br>
                                        Đại diện chi nhánh: Ông . Võ Vĩnh Tiến<br>
                                        Điện thoại : 0934.28.29.33 - 0963.43.89.33<br>
                                        Email : tunghanggialai@gmail.com

                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <strong>Thành Phố Đà Nẵng</strong><br>                                       
                                        An Hải 16 - Quận Sơn Trà - TP Đà Nẵng<br>
                                        Điện thoại: 01279 80 80 90<br>
                                        Email: tunghanggialai@gmail.com
                                    </p>                                    
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="comment-outer">
                        <div id="respond" class="comment-respond">
                            <h2 id="reply-title" class="comment-reply-title">Liên hệ</h2>
                            <form accept-charset='UTF-8' action='https://batakoo-theme.bizwebvietnam.net/contact' id='contact' method='post'>
                                <input name='FormType' type='hidden' value='contact' />
                                <input name='utf8' type='hidden' value='true' />
                                <form class="comment-form">
                                    <p class="comment-form-email">
                                        <label for="author">Họ tên</label>
                                        <span class="required">*</span>
                                        <input id="author" type="text" class="input-text"  name="contact[name]">
                                    </p>
                                    <p class="comment-form-author">
                                        <label for="email">Email</label>
                                        <span class="required">*</span>
                                        <input id="email" type="text" class="input-text" name="contact[email]">
                                    </p>
                                    <p class="comment-form-comment">
                                        <label for="message">Nội dung</label>
                                        <textarea name="contact[body]" id="message" cols="45" rows="10" class="input-text"></textarea>
                                    </p>
                                    <p class="form-submit">
                                        <input class="btn btn-md btn-default" name="submit" type="submit" id="button" value="Gửi liên hệ">
                                    </p>
                                </form>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <aside id="secondary" class="col-md-4">
                <div class="sidebar">
                    <div class="widget">
                        <h3 class="widget-title">Fan page</h3>

                        <div class="fb-page" data-href="https://www.facebook.com/tunghangcoffee81" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/tunghangcoffee81"><a href="https://www.facebook.com/tunghangcoffee81">Tùng Hằng Coffee</a></blockquote></div></div>
                    </div>


<!--                    <div class="widget post-type-widget">-->
<!--                        <h3 class="widget-title">Bài viết nổi bật nhất</h3>-->
<!--                        <ul>-->
<!--                            --><?php
//                            if (!empty($hot)){
//                                foreach($hot as $hot){
//                                    ?>
<!--                                    <li>-->
<!--                                <span class="post-category">-->
<!--                                </span>-->
<!--                                        <figure class="post-thumbnail">-->
<!--                                            <a href="#"><img src="--><?php //echo $hot->image;?><!--"></a>-->
<!--                                        </figure>-->
<!--                                        <h2 class="post-title">-->
<!--                                            <a href="#">--><?php //echo $hot->title;?><!--</a>-->
<!--                                        </h2>-->
<!--                                    </li>-->
<!--                                    --><?php
//                                }
//                            }
//                            ?>
<!---->
<!---->
<!---->
<!--                        </ul>-->
<!--                    </div>  -->
                </div>
            </aside>
        </div>
    </div>
</div>
<script type="application/javascript">
    $(".menu li").removeClass("active");
    $(".menu #contact").addClass("active");
</script>