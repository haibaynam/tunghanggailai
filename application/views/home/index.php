<div id="carousel-home" class="carousel slide" data-ride="carousel">  <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-home" data-slide-to="1"></li>
        <li data-target="#carousel-home" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img onclick="window.location.href='#'" src="<?=base_url("/uploads/images/slide1.jpg");?>">
        </div>
        <div class="item">
            <img onclick="window.location.href='#'" src="<?=base_url("/uploads/images/slide2.jpg");?>">
        </div>
        <div class="item">
            <img onclick="window.location.href='#'" src="<?=base_url("/uploads/images/slide3.jpg");?>">
        </div>
    </div>
    <a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
        <span class="fa fa-fw fa-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
        <span class="fa fa-fw fa-chevron-right"></span>
    </a>
</div>

<section class="home-service">
    <div class="container">
        <div class="row">
            <div class="col-service">
                <span class="small-icon">
                <i class="fa fa-fw fa-coffee"></i>
                </span>
                <div class="small-icon-text">
                    <h4>Sản phẩm chất lượng</h4>
                    <p>Nguyên liệu cà phê đầu vào được lựa chọn kỹ càng và thu mua từ các nhà sản xuất uy tín nhất.</p>
                </div>
            </div>
            <div class="col-service">
                <span class="small-icon">
                 <i class="fa fa-fw fa-leaf"></i>
                </span>
                <div class="small-icon-text">
                    <h4> An toàn cho sức khỏe</h4>
                    <p>Được sản xuất dựa trên quy trình đảm bảo an toàn vệ sinh thực phẩm. Cam kết: Không tẩm hương liệu, phẩm màu; Không sử dụng phụ gia, chất bảo quản.</p>
                </div>
            </div>
            <div class="col-service">
                <span class="small-icon">
                 <i class="fa fa-fw fa-dollar"></i>
                </span>
                <div class="small-icon-text">
                    <h4> Giá thành hợp lý</h4>
                    <p>Đa dạng sản phẩm với các mức giá hợp lý.</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="home-about">
    <div class="container">
        <div class="row">
            <div class="about-text">
                <h3 class="home-title">Về chúng tôi</h3>
                <p class="trigger animated fadeInUp">
                    <?php
                    if (!empty($about_us)){
                        echo $about_us;
                    }
                    ?>
                </p>
                <p><a href="<?=base_url("index.php/Introduction");?>" class="btn btn-default">Xem thêm</a></p>
            </div>
            <div class="about-figure">
                <figure>
                    <img src="<?=base_url("/uploads/images/trusochinh.jpg");?>" style="border-radius: 6px; box-shadow: 0 0 -10px -10px">
                </figure>
            </div>
        </div>
    </div>
</section>


<section class="portfolio">
    <div class="container">
        <div class="row no-gutter">
            <div id="filter">
                <ul id="options">
                    <li><a data-filter="*" href="#" class="selected">Tất cả sản phẩm</a></li>
                </ul>
            </div>
            <!-- end #filter -->
            <div id="foliowrap">x

                <?php
                if (!empty($product)){
                foreach($product as $product){
                ?>
                <figure class="foliobox">

                    <img src="<?php echo base_url($product->image);?>" alt="<?php echo $product->productName;?>">


                                <figcaption>

                                    <h3 class="foltitle"><?php echo $product->productName;?></h3>
                                    <a href="<?=base_url("index.php/products/".$product->alias.".html");?>" class="goto">Xem thêm</a>
                                </figcaption>


                </figure>
                    <?php
                }
                }
                ?>

            </div>
        </div>
    </div>
</section>


<section class="bloghome">
    <div class="container">
        <div class="row">
            <h3 class="home-title">Tin mới</h3>
            <?php
                if (!empty($latest)){
                    foreach($latest as $latest){
                        ?>
                        <div class="col-sm-4">
                            <div class="inner">
                                <figure class="w-thumb">

                                    <a href="<?=base_url("index.php/news/".$latest->alias.".html");?>"><img src="<?php echo base_url($latest->image);?>" alt="<?php echo $latest->title;?>" width="330" height="206"></a>

                                </figure>
                                <div class="entry-header">
                                    <time class="published" datetime="<?php
                                    $now = time();
                                    echo timespan($latest->created, $now);
                                    ?>" >

                                        <?php
                                        $now = time();
                                        echo timespan($latest->created, $now);
                                        ?>
                                    </time>
                                    <h2 class="post-title entry-title">
                                        <a href="<?=base_url("index.php/news/".$latest->alias.".html");?>"><?php echo $latest->title;?></a>
                                    </h2>
                                </div>
                                <div class="entry-content">
                                    <p></p>
                                    <p>
                                        <a href="<?=base_url("index.php/news/".$latest->alias.".html");?>" class="more">Xem thêm</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                <?php
                    }
                }
            ?>

        </div>
    </div>
</section>


<div class="testimonial">
    <div class="container">
        <div class="row">
            <div id="testimonial-home" class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#testimonial-home" data-slide-to="0" class="active"></li>
                    <li data-target="#testimonial-home" data-slide-to="1" class=""></li>
                    <li data-target="#testimonial-home" data-slide-to="2" class=""></li>


                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <figure class="ava">
                            <img src="<?=base_url("/uploads/images/ava1.png");?>">
                        </figure>
                        <div class="context">
                            <div class="inner">
                                <blockquote>Cà phê hương vị thơm ngon, dân dã mang đậm bản sắc của núi rừng Tây Nguyên</blockquote>
                                <p class="who">NguyenHuuChot</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <figure class="ava">
                            <img src="<?=base_url("/uploads/images/ava1.png");?>">
                        </figure>
                        <div class="context">
                            <div class="inner">
                                <blockquote>test</blockquote>
                                <p class="who">Lê Thanh Khang</p>
                            </div>
                        </div>
                    </div>

                </div>
                <a class="left carousel-control" href="#testimonial-home" role="button" data-slide="prev">
                    <span class="fa fa-fw fa-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#testimonial-home" role="button" data-slide="next">
                    <span class="fa fa-fw fa-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    $(".menu li").removeClass("active");
    $(".menu #homepage").addClass("active");
</script>

<!--  <section class="partner">
     <div class="container">
         <div class="row">
             <h3 class="home-title">Đối tác</h3>
             <div class="inner">
                 <div class="col-partner">
                     <div>
                         <img src="">
                     </div>
                 </div>

             </div>
         </div>
     </div>
 </section> -->